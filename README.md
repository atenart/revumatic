# What is Revumatic

Revumatic is a review tool for developers of Red Hat kernels. It provides an
interactive TUI (text user interface) to review GitLab merge requests.

# Features

- provides interactive text user interface
- understands Red Hat kernel specific MR metadata
- compares backports to upstream with highlighted differences
- fetches upstream commits that you don't have locally
- displays missing fixes
- can post your comments
- can approve and unapprove
- shows only relevant comments, hides the noise from CKI bot
- opens Bugzilla with a single key
- shows your GitLab TODO list
- needs no configuration

# Installation

Clone the Revumatic repository:

```
git clone https://gitlab.com/redhat/centos-stream/src/kernel/utils/revumatic.git
cd revumatic
```

Install the required Python dependencies. On Fedora, run:

```
sudo dnf install $(< fedora.txt)
```

On other distros, use `pip3`:

```
pip3 install --user -r requirements.txt
```

To start the tool, just run `./revumatic`. It's recommended to symlink it to
your `/usr/local/bin`:

```
sudo ln -s $(pwd)/revumatic /usr/local/bin/
```

# Usage

## General notes

While Revumatic works outside of the Red Hat VPN, some features are
available only when you are inside the VPN. These include:

- getting missing fixes
- getting upstream commits you don't have locally

These features are dependent on the Kerneloscope server which is not
available publicly.

## From a git repository

Change to the git repository holding your Red Hat kernel. Start Revumatic
with the merge request number:

```
cd ~/git/rhel-8
revumatic 333
```

## From anywhere

Pass the full merge request URL as a parameter:

```
revumatic https://gitlab.com/cki-project/kernel-ark/-/merge_requests/245
```

Revumatic needs a local clone of the kernel git repository. It will ask you
for the path to that repository. It will remember the path and will not ask
you next time you open a MR from the same project.

## Show your TODO list

Start Revumatic with the `todo` parameter:

```
revumatic todo
```

## Show your merge requests

To see the merge requests where you are set as a reviewer:

```
revumatic mine
```

There are options to show the merge requests that you created (`--author`),
that you are assigned to (`--assignee`) and that you are set as a reviewer
(`--reviewer`, the default). For example, to see the merge requests that you
created:

```
revumatic mine --author
```

The options can be combined.

## List and filter all merge requests

Start Revumatic with the `list` parameter. Optionally, specify a filter.
This allows you to search for the merge requests that are of interest to
you. See the help for details and examples:

```
revumatic list --help
```

# FAQ, Tips and Tricks

## UI customization

To change the UI layout, change the colors, or customize key bindings, see
the [UI customization](docs/ui.md) document.

## Adding upstream repos

While Revumatic can fetch upstream commits from the Kerneloscope server,
it's not always practical. Before it reaches out to Kerneloscope, it first
searches all configured local repositories for the commit. Which means you
can just add your local clones of upstream trees to the Revumatic config.

The config file is at `~/.revumatic/config` and the paths should be added to
the `repos` config key, e.g.

```
repos = ~/git/rhel-8
	~/git/rhel-7
	~/git/linus
	~/git/net-text
```

## Subsystem Acks labels in the TODO list

By default, `Acks::NeedsReview` labels are shown in the TODO list and other
NeedsReview labels are hidden. You can customize this behavior to instead
show subsystem labels you are interested in. Add this to the config file
`~/.revumatic/config`:

```
[kernel]
subsystem = subsystem list
```

Separate the subsystems by space. For example, if you add
`subsystem = net tc`, only `Acks::net::NeedsReview` and
`Acks::tc::NeedsReview` will be shown in the TODO list.

Note that this affects the TODO list only, if you open a particular MR,
you'll always see all labels. Also note that this does not filter the merge
requests, it only controls what labels are shown.

This setting may affect more things in the future.

## Do missing fixes account for already backported fixes?

Yes. If a fix is identified but it was already applied earlier to the RHEL
kernel out of order, it will not be shown.

## Are my unsubmitted comments saved when I quit Revumatic?

Yes. Revumatic will not lose your comments. When you open the same MR, your
comments will still be there. If the MR gets changed and rebased meanwhile,
Revumatic will reassign your comments to the new commits.

## Can I have several RH kernels in the same local repo?

Yes. But you will have to start Revumatic with the full merge request URL.
Specifying just a MR number will not work as Revumatic does not know what
kernel the MR number belongs to.

The todo mode (`revumatic todo`) works without problems.

## Can Revumatic be used for other packages than the kernel?

Revumatic is ready for that. It is modular and parsing of metadata of other
packages is just a matter of implementing a single class. Right now, only
the kernel parsing is implemented.

# License

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.
