# Revumatic UI customization

## Large terminal

The standard layout is suitable for small to mid-size screens. If you have
a large terminal (e.g. 200 columns and/or 100 rows), you may find it
impractical. You can specify a different layout in the config file
`~/.revumatic/config`:

```
[mr]
layout = name
```

The currently available layouts are `standard` (the default), `wide` and
`large`.

## Colors

The default color theme forces black background. This may not be what you
prefer. To switch to a different color theme, add this to the config file
`~/.revumatic/config`:

```
[global]
theme = name
```

The currently available themes are `black` (the default) and
`solarized-light` (for terminals using Solarized palette with light
background). Contributions are welcome.

## Custom key bindings

You can customize the key bindings by running:

```
revumatic config-keys
```

## Visualize tabs and trailing whitespace

You can set what characters will be used to render tab and trailing spaces
in commit diffs. Add this to the config file `~/.revumatic/config`:

```
[format]
tab = »·
trail = ·
```

The `tab` value is one or two characters. Every tab is rendered
as the first character followed by a repetition of the second character. If
the second character is omitted, a space will be used.

The `trail` value is one character. A trailing space will be replaced by
that character.

## Swap downstream and upstream in commit comparison

By default, the backported commit goes first (i.e., left or up). If you'd
rather want the upstream commit to be the first one, set this in the config
file `~/.revumatic/config`:

```
[mr]
swap-compare = yes
```

## Sort TODO items

By default TODO items are sorted by reverse creation date (the TODO item one).
You can define your own sorting logic in the config file `~/.revumatic/config`.

Valid values are `created`, `mr_created`, `mr_updated` and `mr_id`.
`reverse` can be added to reverse the order. The default value is `reverse
created`.

```
[todo]
sort-by = mr_created
```
