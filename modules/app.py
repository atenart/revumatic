#!/usr/bin/python3
import argparse
import configparser
import functools
import os
import subprocess
import urwid

from modules.settings import settings
from modules import (
    config_keys,
    git,
    gitlab,
    kernel,
    list_filtered,
    list_mine,
    list_todo,
    mr,
    packages,
    screen,
    theme,
    ui,
    version,
)
all_screens = (list_todo, list_mine, list_filtered, config_keys)


class RunError(Exception):
    pass


class App:
    CONFIG_DIR = '~/.revumatic'
    defconfig = {
        'global': {
            'gitlab': 'gitlab.com',
            'web-browser': 'xdg-open',
        }
    }

    def __init__(self):
        self._on_start = []
        self._timers = {}
        self.loop = None
        self.parse_cmd_line()
        self.init_lowlevel()
        self.read_config()
        self.init_ui()
        self.ui_check_config()

    def start(self):
        try:
            self.lab = gitlab.Gitlab(self.config['global']['gitlab'],
                                     self.config['global']['token'])
        except gitlab.AuthenticationError:
            self.ui_get_token('GitLab access denied.')
            return
        packages.registry.set_config(self.config, self.lab)
        if 'rh_vpn' not in self.config['global'] and \
           self.lab.me['public_email'].endswith('@redhat.com'):
            self.config['global']['rh_vpn'] = 'yes'
            self.config.changed = True
        # self.config could also be changed by the package's set_config
        if self.config.changed:
            self.save_config()

        self.start_screen(self.args.cls, self.args)

    def parse_cmd_line(self):
        base_parser = argparse.ArgumentParser(add_help=False)
        base_parser.add_argument('-c', '--config-dir', default=self.CONFIG_DIR,
                                 help='specify a different config directory ' +
                                      '(default: {}).'.format(self.CONFIG_DIR))

        parser = argparse.ArgumentParser(parents=[base_parser])
        subparsers = parser.add_subparsers(dest='action', metavar='ACTION')
        subparsers.required = True

        # a dummy MR entry for help
        mr_help = 'MR_ID|MR_URL'
        sp_mr = subparsers.add_parser(mr_help, help='merge request id or merge request URL')

        for mod in all_screens:
            cls = getattr(mod, 'Screen')
            kwargs = {}
            if cls.help_raw:
                kwargs['formatter_class'] = argparse.RawDescriptionHelpFormatter
            sp = subparsers.add_parser(cls.command, help=cls.help, epilog=cls.help_epilog,
                                       **kwargs)
            cls.add_arguments(sp)
            sp.set_defaults(cls=cls)

        try:
            import argcomplete
            argcomplete.autocomplete(parser)
        except ImportError:
            pass

        # first, parse only the common option
        self.args, rest = base_parser.parse_known_args()
        # is the parameter a MR?
        if len(rest) == 1:
            if rest[0].startswith('http') or rest[0].isnumeric():
                self.args.action = 'mr'
                self.args.cls = mr.Screen
                self.args.mr_id = rest[0]
                return
        # hide our help hack
        if len(rest) and rest[0] == mr_help:
            raise RunError('Specify an actual MR id or MR URL, not a literal "{}" string.'.format(mr_help))
        # reparse with the complete parser
        self.args = parser.parse_args()

    def get_config_dir(self):
        return os.path.expanduser(self.args.config_dir)

    def init_lowlevel(self):
        directory = self.get_config_dir()
        os.makedirs(directory, exist_ok=True)
        self.lock_file = open(os.path.join(directory, '.lock'), 'w')
        try:
            os.lockf(self.lock_file.fileno(), os.F_TLOCK, 0)
        except BlockingIOError:
            raise RunError('Already running.')

    def done_lowlevel(self):
        os.remove(self.lock_file.name)
        self.lock_file.close()

    def read_config(self):
        self.config = configparser.ConfigParser(default_section=None)
        self.config.read_dict(self.defconfig)
        self.config.read(os.path.join(self.get_config_dir(), 'config'))
        self.config.changed = False

    def save_config(self):
        path = os.path.join(self.get_config_dir(), 'config')
        new_file = open(path + '.new', 'w')
        self.config.write(new_file)
        new_file.close()
        os.rename(path + '.new', path)
        self.config.changed = False

    def get_git_paths(self):
        if not 'repos' in self.config['global']:
            return []
        return self.config['global']['repos'].split('\n')

    def add_git_path(self, path):
        if not 'repos' in self.config['global']:
            self.config['global']['repos'] = path
        else:
            self.config['global']['repos'] += '\n' + path
        self.save_config()

    def ui_get_token(self, msg):
        msg = (msg + ' Go to ' +
               'https://{}/-/profile/personal_access_tokens'.format(self.config['global']['gitlab']) +
               ', select "api" scope and create a token.\nPaste the token:')
        old_token = self.config.get('global', 'token', fallback='')
        self.edit_popup(functools.partial(self.ui_store_config, 'token'), msg, old_token)

    def ui_check_config(self):
        if 'token' not in self.config['global']:
            self.ui_get_token('GitLab Personal Access Token is needed.')
        else:
            if self.config.changed:
                self.save_config()
            self.on_start(self.start)

    def ui_store_config(self, name, text):
        self.config['global'][name] = text
        self.config.changed = True
        self.ui_check_config()

    def on_start(self, callback):
        if self.loop:
            callback()
        else:
            self._on_start.append(callback)

    def init_ui(self):
        class Widgets:
            pass

        settings.apply_config(self.config)

        self.screens = []
        self.widgets = Widgets()

        self.workplace = ui.Workplace(urwid.SolidFill(' '))

        self.default_header = ' {}: {}'.format(version.get_name(), version.get_desc())
        self.widgets.header = urwid.Text(self.default_header, wrap='ellipsis')
        self.widgets.header_status = urwid.Text('')
        self.widgets.footer = urwid.Text('', wrap='clip')

        header_line = urwid.Columns((self.widgets.header,
                                     ('pack', self.widgets.header_status)))
        self.widgets.toplevel = urwid.Frame(urwid.AttrMap(self.workplace, 'std'),
                                            urwid.AttrMap(header_line, 'header'),
                                            urwid.AttrMap(self.widgets.footer, 'footer'))

        self.load_theme()

        urwid.connect_signal(self.workplace, 'focus', self.update_help)
        urwid.connect_signal(self.workplace, 'help', self.show_long_help)
        urwid.connect_signal(packages.registry, 'status-changed', self.set_header_status)
        self.workplace.refocus()

    def load_theme(self):
        self.scr = urwid.raw_display.Screen()
        cur_theme = self.config.get('global', 'theme', fallback='black')
        theme_cls_name = ''.join(t.capitalize() for t in cur_theme.split('-')) + 'Theme'
        try:
            theme_class = getattr(theme, theme_cls_name)
        except AttributeError:
            raise RunError('Unknown theme \'{}\'.'.format(cur_theme))

        self.palette = theme_class(self.scr).palette

        self.focus_map = { p[0][2:]: p[0] for p in self.palette if p[0].startswith('a:') }
        self.focus_map[None] = 'a:std'
        self.passive_map = { p[0][2:]: p[0] for p in self.palette if p[0].startswith('p:') }
        self.passive_map[None] = 'p:std'

        self.overlay_map = { p[0]: 'dimmed' for p in self.palette }
        self.overlay_map[None] = 'dimmed'

    def update_help(self):
        self.widgets.footer.set_text(self.workplace.get_help())

    def show_long_help(self, data):
        if data:
            data = [('title', 'Keyboard shortcuts in this panel:\n\n')] + data
        # add help from the current screen
        if self.screens:
            screen = self.screens[-1]['screen']
            extra = screen.get_long_help()
            if extra:
                if data:
                    data.append('\n')
                data.extend(extra)
        # add help from all packages
        extra = list(packages.registry.collect(lambda x: x.get_extra_help()))
        if extra and data:
            data.append('\n')
        data.extend(extra)
        if not data:
            return
        data = [('title', '{} {}\n\n'.format(version.get_name(),
                                             version.get_version()))] + data
        self.view_popup(None, data)

    def start_screen(self, screen_class, args=None, **kwargs):
        if args is None:
            class Args:
                def __init__(self):
                    for k, v in kwargs.items():
                        setattr(self, k, v)
            args = Args()
        screen = screen_class(self, args)
        self.screens.append({ 'screen': screen })
        self.set_header(self.default_header)
        screen.start()

    def new_action_tracker(self, callback):
        return ui.ActionTracker(self.confirm, callback)

    def _wrap_popup_callback(self, callback):
        def wrapper(*args, **kwargs):
            self.update_help()
            self.loop.draw_screen()
            if callback:
                callback(*args, **kwargs)

        return wrapper

    def popup(self, callback=None, callback_args=None, **kwargs):
        return ui.Popup(self._wrap_popup_callback(callback),
                        self.workplace,
                        callback_args=callback_args,
                        overlay_attr_map=self.overlay_map,
                        **kwargs)

    def edit_popup(self, callback, text, edit_text='', allow_esc=False,
                   callback_args=None):
        """The callback gets the text in the 'text' keyword argument."""
        popup = self.popup(callback, callback_args)
        popup.set_default_button(True, allow_esc)
        widget = urwid.Pile((('pack', urwid.Text(text)),
                             ('pack', popup.new_edit('text', edit_text))))
        popup.start(widget)
        self.update_help()
        return popup

    def buttons_popup(self, callback, text, labels, allow_esc=False,
                      callback_args=None):
        """The callback gets the button in the 'button' keyword argument."""
        popup = self.popup(callback, callback_args, focus_map=self.focus_map)
        if allow_esc:
            popup.set_default_button(False, True)
        buttons = urwid.Columns([('pack', popup.new_button(label)) for label in labels],
                                dividechars=1)
        widget = urwid.Pile((('pack', urwid.Text(text)),
                             ('pack', buttons)))
        popup.start(widget)
        self.update_help()
        return popup

    def message_popup(self, callback, text, callback_args=None):
        popup = self.buttons_popup(callback, text, ('OK',), allow_esc=True,
                                   callback_args=callback_args)
        return popup

    def view_popup(self, callback, markup, callback_args=None):
        popup = self.popup(callback, callback_args, height='full')
        widget = popup.register_widget('help', ui.BoxedEdit(readonly=True))
        widget.set_markup_text(markup)
        popup.set_default_button(False, True)
        popup.start(urwid.AttrMap(widget, { 'help-desc': 'std' }))
        self.update_help()
        return popup

    def wait_popup(self, text):
        popup = self.popup()
        popup.start(urwid.Text(text))
        self.update_help()
        self.loop.draw_screen()
        return popup

    def progress_popup(self, done=100):
        popup = self.popup()
        progress = urwid.ProgressBar('std', 'progress', done=done)
        popup.start(progress)
        self.update_help()
        self.loop.draw_screen()
        return popup

    def progress_update(self, popup, current):
        popup.popup_widget.set_completion(current)
        self.loop.draw_screen()

    def listbox_popup(self, items):
        walker = ui.PrettyListWalker(items, self.focus_map, self.passive_map)
        listbox = ui.TraversableListBox(walker)
        popup = self.popup(popup_widget=listbox, height=len(items))
        popup.set_default_button(False, True)
        return popup, listbox

    def popup_show(self, popup):
        """Only used for popups created with popup_widget set (such as
        listbox_popup), or to show the same popup again."""
        popup.show()
        self.update_help()

    def confirm(self, name, message, callback, *args, reject_callback=None):
        def do_callback(button=None):
            if not button or not button.lower().startswith('yes'):
                if reject_callback:
                    reject_callback()
                return
            if button != 'yes':
                silent.append(name)
                self.config['global']['silent'] = ' '.join(silent)
                self.save_config()
            callback(*args)

        silent = self.config.get('global', 'silent', fallback='').split()
        if name in silent:
            callback(*args)
            return
        popup = self.buttons_popup(do_callback, message,
                                   ['~yes', '~no', '~Yes and don\'t ask again'],
                                   allow_esc=True)

    def redraw_screen(self):
        self.loop.screen.clear()

    def set_workplace(self, widget):
        self.screens[-1]['workplace'] = widget
        self.workplace.original_widget = widget
        self.update_help()

    def set_header(self, title):
        self.screens[-1]['title'] = title
        self.widgets.header.set_text(title)

    def set_header_status(self):
        status = list(packages.registry.collect(lambda x: x.get_status()))
        if status:
            status.insert(0, ' ')
            status.append(' ')
        self.widgets.header_status.set_text(status)

    def quit(self):
        self.screens.pop()
        if not self.screens:
            raise urwid.ExitMainLoop()
        screen = self.screens[-1]
        self.set_header(screen['title'])
        self.set_workplace(screen['workplace'])

    def get_quit_text(self):
        if len(self.screens) == 1:
            return 'quit'
        return 'back'

    def user_command(self, command, parm):
        if isinstance(parm, str):
            parm = (parm, )
        for p in parm:
            subprocess.Popen((command, p))

    def web_browser(self, url):
        self.user_command(self.config['global']['web-browser'], url)

    def _add_timer(self, name, seconds, callback, user_args):
        self._timers[name] = self.loop.set_alarm_in(seconds, self._timer_handler,
                                                    (name, seconds, callback, user_args))

    def _timer_handler(self, loop, data):
        name, seconds, callback, user_args = data
        result = callback(*user_args)
        if result:
            self._add_timer(name, seconds, callback, user_args)
        else:
            del self._timers[name]

    def add_timer(self, name, seconds, callback, *user_args):
        """Will call the callback with user_args after the specified number
        of seconds. The callback may return True to rearm."""
        self.del_timer(name)
        self._add_timer(name, seconds, callback, user_args)

    def del_timer(self, name):
        if name not in self._timers:
            return
        self.loop.remove_alarm(self._timers[name])
        del self._timers[name]

    def keypress(self, key):
        if self.screens:
            screen = self.screens[-1]['screen']
            return screen.keypress(key)
        return False

    def run(self):
        self.loop = ui.EnhancedMainLoop(self.widgets.toplevel,
                                        screen=self.scr,
                                        palette=self.palette,
                                        on_start=self._on_start,
                                        handle_mouse=False,
                                        unhandled_input=self.keypress)
        self.loop.run()
        self.done_lowlevel()


def run():
    app = App()
    try:
        app.run()
    except (screen.ScreenError, git.GitError) as e:
        app.loop.screen.stop()
        raise RunError(str(e))
