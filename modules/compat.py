# This file contains definitions to preserve backwards compatibility.

# Mapping from old action names to the currently used ones. This is used
# when reading the [keys] section of the config file.
compat_actions = {
    'upstream': 'interdiff',
    'upstream_alt': 'interdiff_alt',
}
