import urwid

from modules.settings import settings
from modules import (
    keys,
    screen,
    ui,
)

class Screen(screen.BaseScreen):
    command = 'config-keys'
    help = 'configure the key bindings'

    def __init__(self, app, args):
        super().__init__(app, args)
        self.key_popup = None

    def init_ui(self):
        self.widgets.key_list = ui.TraversableListBox()
        actions = (
            { 'name': 'open', 'prio': 2, 'help': 'assign a key' },
            { 'name': 'remove', 'prio': 2, 'help': 'unassign keys' },
            { 'name': 'quit', 'prio': 0, 'help': self.app.get_quit_text(),
              'confirm': 'Do you want to save the changes and quit?' },
        )
        self.action_tracker.add_actions(self.widgets.key_list, actions)
        self.app.set_header(('header', ' Revumatic configuration: keys '))
        return self.widgets.key_list

    def format_item(self, name):
        result = []
        result.append('{:{}}'.format(name.replace('_', ' '), self.name_len))
        cur_keys = settings.key_map[name]
        if name not in settings.key_map._modified:
            result.append(' not set, default:')
        result.append(' ')
        for i, k in enumerate(cur_keys):
            if i > 0:
                result.append(', ')
            result.append(('help-key', ' {} '.format(keys.display_keys.get(k, k))))
        return result

    def load_data(self):
        self.names = [k for k in dir(keys.Keys) if not k.startswith('_')]
        self.name_len = max(len(n) for n in self.names)
        walker = ui.PrettyListWalker((self.format_item(n) for n in self.names),
                                     self.app.focus_map, self.app.passive_map)
        self.widgets.key_list.body = walker

    def update_item(self, index):
        new_text = self.format_item(self.names[index])
        self.widgets.key_list.body[index].original_widget.set_text(new_text)

    def keypress(self, key):
        if not self.key_popup:
            return False
        self.key_popup.stop()
        self.key_popup = None
        index = self.widgets.key_list.focus_position
        settings.key_map[self.names[index]] = key
        self.update_item(index)
        return True

    def add_key(self):
        self.key_popup = self.app.popup(popup_widget=urwid.SelectableIcon('Press a key'))
        self.app.popup_show(self.key_popup)

    def remove_keys(self):
        index = self.widgets.key_list.focus_position
        del settings.key_map[self.names[index]]
        self.update_item(index)

    def save(self):
        self.app.config['keys'] = {}
        for name in self.names:
            if name not in settings.key_map._modified:
                continue
            values = '\n'.join(keys.config_keys.get(k, k) for k in settings.key_map[name])
            self.app.config['keys'][name.replace('_', '-')] = values
        self.app.save_config()

    def action(self, what):
        if what == 'quit':
            self.save()
            self.app.quit()
        elif what == 'open':
            self.add_key()
        elif what == 'remove':
            self.remove_keys()
