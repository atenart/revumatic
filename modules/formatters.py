import collections
import datetime
import difflib
import itertools
import re

from modules.settings import settings


def commit_name(commit):
    try:
        return commit.message[:commit.message.index('\n')]
    except ValueError:
        return commit.message


def replace_tabs(txt, offset=0):
    result = []
    pos = offset
    for c in txt:
        if c == '\n':
            result.append(c)
            pos = offset
        elif c == '\t':
            if pos < 0:
                result.append('?')
                pos += 1
            else:
                add = 8 - pos % 8
                result.append(settings.format_tab[0] +
                              settings.format_tab[1] * (add - 1))
                pos += add
        else:
            result.append(c)
            pos += 1
    return ''.join(result), pos


def replace_tabs_trail(txt, offset=0):
    txt = replace_tabs(txt, offset)[0]
    if txt.endswith(' '):
        txt = txt[:-1] + settings.format_trail[0]
    return txt


def join_lines(lines):
    return '\n'.join(itertools.chain(lines, ('',)))


class DiffList(collections.UserList):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chars = 0
        self.line_starts = []
        self.highlights = []
        self.last_highlighted = False

    def _add_chars(self, item, highlighted):
        s = item if type(item) == str else item[1]
        if s == '':
            return
        if not self.line_starts:
            self.line_starts.append(0)

        if highlighted:
            line = len(self.line_starts) - 1
            if self.last_highlighted:
                self.highlights[-1][1] = line
            else:
                self.highlights.append([line, line, self.chars])
        self.last_highlighted = highlighted

        idx = 0
        while True:
            idx = s.find('\n', idx)
            if idx < 0:
                break
            idx += 1
            self.line_starts.append(self.chars + idx)

        self.chars += len(s)

    def append(self, item, highlighted=False):
        self._add_chars(item, highlighted)
        super().append(item)

    def extend(self, items, highlighted=False):
        for item in items:
            self._add_chars(item, highlighted)
        super().extend(items)

    def lines(self):
        return len(self.line_starts)


def merge_highlights(list1, list2):
    def overlaps(place1, place2):
        if not place1 or not place2:
            return False
        return ((place1[0] >= place2[0] and place1[0] <= place2[1]) or
                (place2[0] >= place1[0] and place2[0] <= place1[1]))

    result = []
    h1 = list1.highlights
    h2 = list2.highlights
    pos1 = pos2 = 0
    while True:
        cur1 = cur2 = None
        if pos1 < len(h1):
            cur1 = h1[pos1]
        if pos2 < len(h2):
            cur2 = h2[pos2]
        if not cur1 and not cur2:
            break
        if overlaps(cur1, cur2):
            # areas overlap
            result.append((cur1[2], cur2[2]))
            pos1 += 1
            pos2 += 1
            continue
        # areas don't overlap, try to match to the previous area
        if pos2 > 0 and overlaps(cur1, h2[pos2 - 1]):
            result.append((cur1[2], h2[pos2 - 1][2]))
            pos1 += 1
            continue
        if pos1 > 0 and overlaps(h1[pos1 - 1], cur2):
            result.append((h1[pos1 - 1][2], cur2[2]))
            pos2 += 1
            continue
        # still no overlap, record the lower position
        if not cur2 or (cur1 and cur1[0] < cur2[0]):
            y = cur1[0]
            x = cur1[2] - list1.line_starts[y]
            try:
                line_len = list2.line_starts[y + 1] - list2.line_starts[y] - 1
            except IndexError:
                line_len = list2.chars - list2.line_starts[y]
            x = min(x, line_len)
            result.append((cur1[2], list2.line_starts[y] + x))
            pos1 += 1
            continue
        if not cur1 or (cur2 and cur2[0] < cur1[0]):
            y = cur2[0]
            x = cur2[2] - list2.line_starts[y]
            try:
                line_len = list1.line_starts[y + 1] - list1.line_starts[y] - 1
            except IndexError:
                line_len = list1.chars - list1.line_starts[y]
            x = min(x, line_len)
            result.append((list1.line_starts[y] + x, cur2[2]))
            pos2 += 1
            continue
        assert False
    return result


class AnnotationFormatter:
    def __init__(self, default_cls, maxcols=None):
        self._default_cls = default_cls
        self._maxcols = maxcols
        self._lines = []
        self._line_length = 0
        self._extra_breaks = 0

    def add(self, markup, label=False):
        if self._extra_breaks:
            self._lines.append((self._default_cls, '\n' * self._extra_breaks))
            self._extra_breaks = 0
            self._line_length = 0
        if not isinstance(markup, list):
            markup = [markup]
        txt_len = sum(len(m) if type(m) == str else len(m[1]) for m in markup)
        if label:
            txt_len += 2
        if self._maxcols is not None and self._line_length + txt_len + 1 > self._maxcols:
            self.add_break()
        if self._line_length:
            self._lines.append((self._default_cls, ' '))
            self._line_length += 1
        for i, m in enumerate(markup):
            if type(m) == str:
                cls = self._default_cls
                txt = m
            else:
                cls, txt = m
            if label and i == 0:
                txt = '\u00a0' + txt
            if label and i == len(markup) - 1:
                txt = txt + '\u00a0'
            self._lines.append((cls, txt))
        self._line_length += txt_len

    def add_break(self):
        """Will not be added if it's already there."""
        if self._line_length:
            self._lines.append((self._default_cls, '\n'))
            self._line_length = 0

    def add_empty_line(self):
        """Will not be added at the beginning and end of the annotations."""
        if not self._lines:
            return
        self.add_break()
        self._extra_breaks += 1

    def format(self):
        if not self._lines:
            return None
        return self._lines + [(self._default_cls, '\n')]


class CommitFormatter:
    def __init__(self, commit, diff, compared_sha=None, compared_titles=None):
        self._commit = commit
        self._diff = diff
        self._compared_titles = compared_titles or ('backport', 'upstream')
        self._compared_sha = compared_sha
        if type(self._compared_sha) == str:
            self._compared_sha = (str(self._commit.oid)[:12], compared_sha[:12])

    def _abbrev(self, commit_id):
        return str(commit_id)[:12]

    def _email(self, sig):
        return '{} <{}>'.format(sig.name, sig.email)

    def _date(self, sig):
        tz = datetime.timezone(datetime.timedelta(minutes=sig.offset))
        return datetime.datetime.fromtimestamp(sig.time, tz)

    def _date_str(self, sig):
        return self._date(sig).strftime('%c %z')

    def name(self):
        return commit_name(self._commit)

    def header(self):
        if self._commit is None:
            return self._diff.description
        if isinstance(self._commit, str):
            return self._commit

        msg = self._commit.message
        if msg.endswith('\n'):
            msg = msg[:-1]
        msg = re.sub('^', '    ', msg, flags=re.MULTILINE)

        merge = ''
        if len(self._commit.parent_ids) > 1:
            merge = 'Merge: {}\n'.format(' '.join([self._abbrev(p) for p in self._commit.parent_ids]))

        return 'commit {}\n{}Author: {}\nDate:   {}\n\n{}'.format(self._commit.id, merge,
               self._email(self._commit.author), self._date_str(self._commit.author),
               replace_tabs(msg)[0])

    def stat(self, width=80):
        def clip_path(path, path_len):
            if len(path) <= path_len:
                return path
            return path[:4] + '…' + path[len(path)-path_len+5:]

        files = len(self._diff)
        if not files:
            return None

        name_len = max(len(f.new_name) if f.old_null else len(f.name) for f in self._diff)
        name_len = min(name_len, width // 2 - 2)
        change_len = len(str(self._diff.max_changes))
        stat_len = width - 5 - name_len - change_len
        if self._diff.max_changes == 0:
            unit = 0
        else:
            unit = min(stat_len / self._diff.max_changes, 1)

        result = []
        total_add = 0
        total_del = 0
        for f in self._diff:
            name = clip_path(f.new_name if f.old_null else f.name, name_len)
            line = ' {:{}} | {:-{}} '.format(name, name_len, f.changes, change_len)
            if f.added:
                line += '+' * max(int(f.added * unit), 1)
            if f.removed:
                line += '-' * max(int(f.removed * unit), 1)
            result.append(line)
            if f.name != f.new_name and not f.old_null and not f.new_null:
                new_name = clip_path(f.new_name, width - 5 - name_len)
                result.append(' {:{}} => {}'.format('', name_len, new_name))

            total_add += f.added
            total_del += f.removed

        total = ' {} file{} changed'.format(files, 's' if files > 1 else '')
        if total_add:
            total += ', {} insertion{}(+)'.format(total_add, 's' if total_add > 1 else '')
        if total_del:
            total += ', {} deletion{}(+)'.format(total_del, 's' if total_del > 1 else '')
        result.append(total)
        return '\n'.join(result)

    def format_header(self):
        return [('desc', self.header() + '\n')]

    def format_stat(self):
        stat = self.stat()
        if stat:
            return [('desc', self.stat() + '\n')]
        return None

    def format_diff(self):
        result = DiffList()
        self._add_diff(result)
        return result.data

    def format_diff_line(self, path, line_no, old, count=3):
        result = []
        lines = self._diff.get_line(path, line_no, old, count)
        if not lines:
            return None
        result = []
        for line in lines:
            self._add_line(result, line)
        return result

    def get_file_list(self):
        result = []
        for f in self._diff:
            if not f.old_null:
                result.append(f.name)
            if f.name != f.new_name and not f.new_null:
                result.append(f.new_name)
        return result

    def _add_file_header(self, result, f):
        for hdr in f.headers:
            result.append(('diff-file', hdr + '\n'))
        result.append(('diff-file', '--- {}\n'.format('/dev/null' if f.old_null else 'a/'+f.name)))
        result.append(('diff-file', '+++ {}\n'.format('/dev/null' if f.new_null else 'b/'+f.new_name)))

    def _add_hunk_header(self, result, h):
        parts = h.header.split('@@')
        func = parts[2]
        parts[2] = ''
        result.append(('diff-hunk', '@@'.join(parts)))
        result.append(('diff-func', func + '\n'))

    def _add_line(self, result, l):
        if l.origin == '+':
            cls = 'diff-add'
        elif l.origin == '-':
            cls = 'diff-del'
        else:
            cls = 'diff'
        result.append((cls, '{}{}\n'.format(l.origin, replace_tabs_trail(l.content))))

    def _add_partial_line(self, highlight, highlight_origin,
                          result, l, disp_offset, start, count=None):
        if count is not None and count <= 0:
            return disp_offset

        if l.origin == '+':
            origin_cls = 'diff-add'
        elif l.origin == '-':
            origin_cls = 'diff-del'
        else:
            origin_cls = 'diff'
        cls = origin_cls
        if highlight:
            cls += '-miss'
        if highlight_origin:
            origin_cls += '-miss'
        if count is None or start + count > len(l.content):
            if start == 0:
                result.append((origin_cls, l.origin), highlighted=highlight_origin)
            result.append((cls, '{}\n'.format(replace_tabs_trail(l.content[start:],
                                                                 disp_offset))),
                          highlighted=highlight)
            return 0
        assert start + count <= len(l.content)
        s2, disp_offset = replace_tabs(l.content[start:start+count], disp_offset)
        if start == 0:
            result.append((origin_cls, l.origin))
        result.append((cls, s2), highlighted=highlight)
        return disp_offset

    def _add_diff(self, result):
        for f in self._diff:
            self._add_file_header(result, f)
            for h in f:
                self._add_hunk_header(result, h)
                for l in h:
                    self._add_line(result, l)
        return result

    def _sync(self, left, right):
        left_lines = left.lines()
        right_lines = right.lines()
        if left_lines < right_lines:
            left.extend((('diff-miss', '⦚\n'),) * (right_lines - left_lines),
                        highlighted=True)
        elif right_lines < left_lines:
            right.extend((('diff-miss', '⦚\n'),) * (left_lines - right_lines),
                         highlighted=True)

    def _compare_left_right(self, left, right, h):
        def get_highlight_origin():
            try:
                return h.left[pos_line[0]].origin != h.right[pos_line[1]].origin
            except IndexError:
                return False

        def output_completed_line(highlight, highlight_origin, where, hh, idx, count):
            if count <= 0:
                return False, count
            remaining = len(hh[pos_line[idx]].content) - offs_line[idx]
            if count < remaining + 1:
                return False, count
            self._add_partial_line(highlight, highlight_origin,
                                   where, hh[pos_line[idx]],
                                   disp_offs_line[idx], offs_line[idx])
            count -= remaining + 1  # the added 1 is for the \n
            pos_line[idx] += 1
            offs_line[idx] = disp_offs_line[idx] = 0
            return True, count

        def output_incomplete_line(highlight, highlight_origin, where, hh, idx, count):
            if count <= 0:
                return count
            disp_offs_line[idx] = self._add_partial_line(highlight, highlight_origin,
                                                         where, hh[pos_line[idx]],
                                                         disp_offs_line[idx],
                                                         offs_line[idx], count)
            offs_line[idx] += count
            return 0

        def cond_sync():
            # if we stayed in the middle of the left or right line, we'll
            # have to wait with the sync, we can't insert a break there
            if offs_line[0] == 0 and offs_line[1] == 0:
                self._sync(left, right)

        blocks = difflib.SequenceMatcher(a=join_lines(l.content for l in h.left),
                                         b=join_lines(l.content for l in h.right),
                                         autojunk=False).get_matching_blocks()
        # index of the currently processed line on the left and right
        # side:
        pos_line = [0, 0]
        # current offsets in pos_line:
        offs_line = [0, 0]
        # the same as offs_line but after tab expansion:
        disp_offs_line = [0, 0]
        # index of the currently processed block from the SequenceMatcher:
        cur_block_idx = 0
        # the previously processed block:
        last_block = (0, 0, 0)
        # number of characters that needs to be consumed
        x_l = x_r = 0
        while True:
            n_l, n_r, n = blocks[cur_block_idx]
            if n < 5 and cur_block_idx < len(blocks) - 1:
                cur_block_idx += 1
                continue
            x_l += n_l - (last_block[0] + last_block[2])
            x_r += n_r - (last_block[1] + last_block[2])

            repeat_l = repeat_r = True
            while repeat_l or repeat_r:
                hl_origin = get_highlight_origin()
                repeat_l, x_l = output_completed_line(True, hl_origin, left, h.left, 0, x_l)
                repeat_r, x_r = output_completed_line(True, hl_origin, right, h.right, 1, x_r)
            # sync after outputting all differing lines
            cond_sync()

            hl_origin = get_highlight_origin()
            x_l = output_incomplete_line(True, hl_origin, left, h.left, 0, x_l)
            x_r = output_incomplete_line(True, hl_origin, right, h.right, 1, x_r)

            if n == 0:
                break

            x_l += n
            x_r += n
            repeat_l = repeat_r = True
            while repeat_l or repeat_r:
                hl_origin = get_highlight_origin()
                repeat_l, x_l = output_completed_line(False, hl_origin, left, h.left, 0, x_l)
                repeat_r, x_r = output_completed_line(False, hl_origin, right, h.right, 1, x_r)
                # sync after outputting the first matching line; this is
                # necessary if we were not able to sync earlier
                cond_sync()

            hl_origin = get_highlight_origin()
            x_l = output_incomplete_line(False, hl_origin, left, h.left, 0, x_l)
            x_r = output_incomplete_line(False, hl_origin, right, h.right, 1, x_r)

            last_block = blocks[cur_block_idx]
            cur_block_idx += 1

    def format_compared(self):
        if not self._diff.was_compared():
            return None
        left = DiffList()
        right = DiffList()
        if self._compared_sha:
            left.append(('diff-subject', ' {} '.format(self._compared_titles[0])))
            left.append(('diff-sha', ' {}\n'.format(self._compared_sha[0])))
            right.append(('diff-subject', ' {} '.format(self._compared_titles[1])))
            right.append(('diff-sha', ' {}\n'.format(self._compared_sha[1])))
        for f in self._diff.compared():
            if f.left:
                self._add_file_header(left, f)
            if f.right:
                self._add_file_header(right, f.other_file or f)
            self._sync(left, right)
            for h in f.compared():
                if h.left:
                    self._add_hunk_header(left, h.left)
                if h.right:
                    self._add_hunk_header(right, h.right)
                if h.left and h.right and h.left.fuzz:
                    self._compare_left_right(left, right, h)
                else:
                    if h.left:
                        for l in h.left:
                            self._add_line(left, l)
                    if h.right:
                        for l in h.right:
                            self._add_line(right, l)
                self._sync(left, right)
        return left.data, right.data, merge_highlights(left, right)


class CommentFormatter:
    def __init__(self, pkg):
        self.pkg = pkg

    def format(self, commit_id, name, comment, number=None, upstream=None, prefix=None):
        header = []
        if prefix:
            header.append('[{}]'.format(prefix))
        if number is not None:
            header.append('[{}/{}]'.format(*number))
        header.append('`{:.12}`'.format(commit_id))
        header.append('("{}")'.format(name))
        if upstream:
            header.append('[upstream:`{:.12}`]'.format(upstream))
        comment = self.pkg.postformat_comment(comment.rstrip('\n').replace('\n', '\n  '),
                                              True)
        return '- {}\n\n  {}'.format(' '.join(header), comment)

    def format_main(self, comment):
        return self.pkg.postformat_comment(comment.rstrip('\n'), False)

    def join(self, formatted_list):
        return '\n\n'.join(formatted_list)
