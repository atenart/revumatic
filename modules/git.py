import datetime
import os
import pygit2
import subprocess
import urllib.parse

from modules import gitlab, packages, patch


def is_kernel_project(project_name):
    return any(project_name.startswith(p) for p in PROJECTS)


class GitError(Exception):
    pass

class GitFatalError(GitError):
    pass

class GitConfigError(GitError):
    def __init__(self, msg, suggested_command=None):
        super().__init__(msg)
        self.suggested_command = suggested_command

class GitNoRemote(GitError):
    pass

class GitBadProject(GitError):
    pass


class Repo:
    def __init__(self, path):
        self.path = os.path.expanduser(path)
        try:
            self.repo = pygit2.Repository(self.path)
        except pygit2.GitError:
            raise GitFatalError('\'{}\' is not a git repository.'.format(path))

    def find_remote(self, labname, projectname=None):
        found_remotes = []
        found_project = None
        if projectname:
            pkg = packages.find(projectname)
            if not pkg:
                raise GitBadProject('Unsupported project: {}'.format(projectname))
        for remote in self.repo.remotes:
            if remote.url.startswith('http'):
                parsed = urllib.parse.urlsplit(remote.url)
                server = parsed.netloc
                project = parsed.path
                if project.startswith('/'):
                    project = project[1:]
            else:
                try:
                    server, project = remote.url.split(':')
                    login, server = server.split('@')
                except ValueError:
                    continue
            if project.endswith('.git'):
                project = project[:-4]
            if projectname and project != projectname:
                continue
            if server == labname:
                if not projectname:
                    pkg = packages.find(project)
                    if not pkg:
                        continue
                found_remotes.append(remote)
                if found_project is None:
                    found_project = project
                    found_pkg = pkg
                elif found_project != project:
                    found_project = False

        count = 0
        if found_project:
            for remote in found_remotes:
                for ref in remote.fetch_refspecs:
                    if ref.startswith('+'):
                        ref = ref[1:]
                    src, dst = ref.split(':')
                    if src.startswith('refs/merge-requests/') and dst.endswith('/*'):
                        self.remote = remote
                        self.project = found_project
                        self.short_project = found_project.split('/')[-1]
                        self.pkg = found_pkg
                        self.ref = dst[:-2]
                        count += 1

        if count == 1:
            return self.pkg

        if count > 0 or len(found_remotes) != 1 or not found_project:
            if found_remotes:
                if projectname:
                    msg = 'Multiple remotes matching {}'.format(projectname)
                else:
                    msg = 'Multiple remotes with a downstream project'
            else:
                if projectname:
                    msg = 'No remote matching {}'.format(projectname)
                else:
                    msg = 'No remote with a downstream project'
            raise GitNoRemote('{} found in repository \'{}\'.'.format(msg, self.path))

        cmd = ['git']
        if self.path != '.':
            cmd.extend(('-C', self.path))
        cmd.extend(('config', '--add', 'remote.{}.fetch'.format(found_remotes[0].name),
                    '+refs/merge-requests/*/head:refs/remotes/{}/merge-requests/*'.format(found_remotes[0].name)))
        raise GitConfigError('No refspec with merge requests found. Please run:\n' + ' '.join(cmd), cmd)

    def fetch(self, commit=None, silent=False):
        args = ['git', '-C', self.path, 'fetch', self.remote.name]
        if commit is not None:
            args.append(commit)
        result = subprocess.run(args, check=not silent)
        return result.returncode == 0

    def full_sha(self, revision):
        return str(self.repo.revparse_single(revision).oid)

    def get_mr_head(self, mr_id):
        return self.full_sha('{}/{}'.format(self.ref, mr_id))

    def get_commits(self, base, head):
        try:
            head = self.repo.revparse_single(head).oid
        except KeyError:
            # try to fetch the head in case it is a historic commit that was
            # not fetched by the standard 'git fetch'
            self.fetch(head, silent=True)
            head = self.repo.revparse_single(head).oid
        base = self.repo.revparse_single(base).oid
        walker = self.repo.walk(head)
        walker.sort(pygit2.GIT_SORT_TOPOLOGICAL | pygit2.GIT_SORT_REVERSE)
        walker.hide(base)
        return walker

    def get_diff(self, commit, silent=False):
        if not isinstance(commit, str):
            commit = str(commit.oid)
        args = ('git', '-C', self.path, 'show', '-p', commit)
        result = subprocess.run(args, check=not silent, universal_newlines=True,
                                stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                                errors='replace')
        return result.stdout

    def get_diff_range(self, base, head):
        args = ('git', '-C', self.path, 'diff', '{}..{}'.format(base, head))
        for i in range(2):
            result = subprocess.run(args, check=False, universal_newlines=True,
                                    stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                                    errors='replace')
            if result.returncode == 0:
                return result.stdout
            # try to fetch the head in case it is a historic commit that was
            # not fetched by the standard 'git fetch'
            if i > 0 or not self.fetch(head, silent=True):
                result.check_returncode()
                assert False

    def get_parsed_diff(self, commit):
        return patch.PatchParser(str(commit.oid)[:12], self.get_diff(commit)).parse()

    def get_parsed_diff_range(self, base, head):
        return patch.PatchParser('{:.12}..{:.12}'.format(base, head),
                                 self.get_diff_range(base, head)).parse()


class RepoCollection:
    def __init__(self, paths, labname):
        self.repos = [Repo(path) for path in paths]
        self.labname = labname

    def add_repo(self, path, check_projectname=None):
        repo = Repo(path)
        if check_projectname:
            repo.find_remote(self.labname, check_projectname)
        self.repos.append(repo)

    def find_repo(self, projectname=None):
        for repo in self.repos:
            try:
                pkg = repo.find_remote(self.labname, projectname)
                return repo, pkg
            except GitNoRemote:
                if not projectname:
                    raise
        return None, None

    def get_diff(self, commit):
        if not isinstance(commit, str):
            commit = str(commit.oid)
        for repo in self.repos:
            if not repo.repo.get(commit):
                continue
            diff = repo.get_diff(commit, silent=True)
            if diff:
                return diff
        return ''


class MRVersion:
    def __init__(self, repo, data, override_base_sha, number, count):
        self.repo = repo
        self.data = data
        self.base_sha = override_base_sha
        self.number = number
        self.count = count
        self.created_at = gitlab.parse_datetime(data['created_at'])
        self.commits = None
        self.parsed_diff = None

    def set_shas(self, project, mr, activity):
        if self.number == self.count:
            self.base_sha = mr['diff_refs']['base_sha']
            self.head_sha = mr['diff_refs']['head_sha']

            new_base = self.repo.pkg.get_base_sha(mr)
            if new_base:
                try:
                    self.base_sha = self.repo.full_sha(new_base)
                except KeyError:
                    pass
            return

        if not self.base_sha:
            self.base_sha = self.data['base_commit_sha']
        self.head_sha = self.data['head_commit_sha']
        new_base = self.repo.pkg.get_historical_base_sha(self.number, activity)
        if not new_base:
            return
        if len(new_base) == 40:
            self.base_sha = new_base
            return
        try:
            self.base_sha = self.repo.full_sha(new_base)
            return
        except KeyError:
            pass
        # The commit is not available locally. We need to query the API
        # for the full commit id. We can fetch an arbitrary commit id
        # from the remote only if we have its full sha.
        self.base_sha = project.commit(new_base)['id']

    def get_commits(self):
        if self.commits is None:
            self.commits = [(c, self.repo.pkg.get_upstream(c))
                            for c in self.repo.get_commits(self.base_sha, self.head_sha)]
        return self.commits

    def get_parsed_diff(self):
        if self.parsed_diff is None:
            self.parsed_diff = self.repo.get_parsed_diff_range(self.base_sha, self.head_sha)
        return self.parsed_diff
