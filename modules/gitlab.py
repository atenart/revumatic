import datetime
import json
import os
import re
import urllib3
from urllib.parse import quote

def parse_datetime(s):
    if s.endswith('Z'):
        s = s[:-1] + '+0000'
    return datetime.datetime.strptime(s, '%Y-%m-%dT%H:%M:%S.%f%z')


class GitlabError(Exception):
    pass

class ConnectionError(GitlabError):
    pass

class ResponseError(GitlabError):
    def __init__(self, status, path, get_fields, msg=None):
        if msg is None:
            msg = str(status)
        self.msg = msg
        self.status = status
        self.path = path
        self.get_fields = get_fields
        msg = '{} ({})'.format(msg, path)
        if get_fields is not None:
            msg = '{} [{}]'.format(msg, str(get_fields))
        super().__init__(msg)

class NotModifiedError(ResponseError):
    pass

class AuthenticationError(ResponseError):
    pass

class NotFoundError(ResponseError):
    pass

class ConflictError(ResponseError):
    pass


class GitlabConnection:
    def __init__(self, host, personal_token, port=443, ca_certs=None):
        self.pool = urllib3.HTTPSConnectionPool(host, port,
                                                cert_reqs='CERT_REQUIRED',
                                                ca_certs=self._find_ca_certs(ca_certs),
                                                headers={'PRIVATE-TOKEN': personal_token})
        self.me = User(self, path='/user')
        # force fetch to verify the connection is working
        self.me.fetch()

    def _find_ca_certs(self, ca_certs):
        if ca_certs is not None:
            paths = (ca_certs,)
        else:
            paths = ('/etc/pki/tls/certs/ca-bundle.crt',
                     '/etc/ssl/certs/ca-certificates.crt')
        for path in paths:
            if os.path.exists(path):
                return path
        raise ConnectionError('No certificate store found')

    def _request(self, method, path, fields, headers, raw):
        if method == 'GET':
            f = self.pool.request_encode_url
            if fields:
                converted = []
                for k, v in fields.items():
                    if isinstance(v, (list, tuple)):
                        k = k + '[]'
                        converted.extend((k, vv) for vv in v)
                    else:
                        converted.append((k, v))
                fields = converted
            kwargs = { 'fields': fields }
        else:
            f = self.pool.urlopen
            kwargs = {
                'headers': { 'Content-Type': 'application/json' },
                'body': json.dumps(fields),
            }
            kwargs['headers'].update(self.pool.headers)
        try:
            resp = f(method, '/api/v4' + path, preload_content=False, **kwargs)
        except urllib3.exceptions.HTTPError as e:
            raise ConnectionError(str(e)) from None
        if resp.status < 200 or resp.status >= 300:
            try:
                data = json.load(resp)
            except json.JSONDecodeError:
                data = {}
            cls = { 304: NotModifiedError,
                    401: AuthenticationError,
                    404: NotFoundError,
                    409: ConflictError,
                  }.get(resp.status, ResponseError)
            raise cls(resp.status, path, fields, data.get('message'))
        if raw:
            data = resp.read()
        else:
            data = json.load(resp)
        if headers:
            return data, resp.headers
        return data

    def get(self, path, parms=None, headers=False, raw=False):
        return self._request('GET', path, parms, headers, raw)

    def post(self, path, parms=None, headers=False, raw=False):
        return self._request('POST', path, parms, headers, raw)

    def put(self, path, parms=None, headers=False, raw=False):
        return self._request('PUT', path, parms, headers, raw)


class List(list):
    """A list of objects. Automatically de-paginates."""

    _link_re = re.compile(r'<([^>]*)>; *rel="next"')
    _path_re = re.compile(r'https://[^/]*/api/v[^/]*(.*)')

    def __init__(self, item_class, conn, provided, path, get_parms=None):
        super().__init__()
        get_parms = {} if get_parms is None else dict(get_parms)
        get_parms['pagination'] = 'keyset'
        get_parms['per_page'] = 100
        while True:
            data_list, headers = conn.get(path, get_parms, headers=True)
            self.extend(item_class(conn, provided, data=data) for data in data_list)
            link = headers.get('Link')
            if not link:
                break
            match = self._link_re.search(link)
            if not match:
                break
            match = self._path_re.match(match.group(1))
            if not match:
                raise ConnectionError('Unknown pagination link {}'.format(link))
            path = match.group(1)
            get_parms = None


class BaseData:
    """A base for objects returned by API calls. The content is lazy
    fetched. Call fetch() to fetch immediatelly.

    _path is used to construct self.path. It can contain {key} or
    {key->key->key} substitions. The substitutions are satisfied from the
    parent's provides, from path_parms and from the object's data, whatever
    is available.

    In the case of {key1->key2->key3} substitions, the keys specify a chain
    of keys to get value from, i.e. self[key1][key2][key3].

    _provides is a dictionary of keys to export to objects to which this one
    is a parent. The dictionary values specify what should be exported; the
    usual {key->key->key} syntax is supported.

    _attrs and _methods allow on demand construction of properties and
    methods. The key is an attribute name, the value is a tuple of (class,
    path, path_parm, path_parm...). The path is provided only for classes
    that do not implement _path (i.e. List and Str classes); for the rest,
    the path must be omitted and it is taken from that class's _path.

    The path can start with a plus sign, in which case the path of the
    current object will be prepended. The path can include {key->key->key}
    parameter substitions.

    path_parms specify what path_parms to pass to the created object. Each
    path_parms is either a parameter name or 'name=value'. For _attrs, only
    the 'name=value' form makes sense. Note that 'name={key->key}' is
    allowed. With no value, the next positional argument passed to the
    created method is assigned to the 'name'.

    keyword args passed to a created method are used as get_parms.

    The class in _attrs and _methods may also be a string with the class
    name. It may also be a list with a class (or with a class name)
    enclosed, in which case a List is fetched."""
    _path = None
    _provides = {}
    _attrs = {}
    _methods = {}

    _parm_re = re.compile(r'\{([^{}]*)\}')

    def __init__(self, conn, provided=None, path_parms=None, get_parms=None,
                 data=None, path=None):
        """parent is another BaseData instance. If data is specified, the
        object is initialized with that data. Otherwise, it is fetched from
        GitLab. In such case, path_parms optionally specifies parameters for
        path substitution and get_parms optionally specifies parameters for
        GET query. Alternatively, a fully construed path may be specified."""
        self.conn = conn
        self.parms = path_parms or {}
        self.get_parms = get_parms
        self.data = data
        self.provided = provided or {}
        self.path = path
        if path is None:
            try:
                self.path = self._construct_path(self._path)
            except KeyError:
                pass

    def fetch(self):
        """Fetches the data from GitLab. Safe to be called repeatedly, once
        fetched, the data is cached."""
        if self.data is None:
            self.data = self.conn.get(self.path, self.get_parms)

    def copy(self, **kwargs):
        """Returns an unfetched copy of self. Useful for re-fetching data
        from GitLab."""
        if self.path is None:
            return None
        result = type(self)(self.conn, self.provided, path_parms=self.parms, get_parms=kwargs)
        result.path = self.path
        return result

    def save(self, override_method=None):
        if not hasattr(self, 'save_data'):
            return
        f = self.conn.post
        if override_method:
            f = getattr(self.conn, override_method)
        self._update_data(f(self.path, self.save_data))
        del self.save_data

    def _update_data(self, new_data):
        if self.data is None:
            return
        self.data.update(new_data)

    def __len__(self):
        self.fetch()
        return len(self.data)

    def get(self, key, fetch=False):
        if fetch:
            self.fetch()
        if self.data and key in self.data:
            return self.data[key]
        if key in self.parms:
            return self.parms[key]
        if key in self.provided:
            return self.provided[key]
        raise KeyError(key)

    def __getitem__(self, key):
        return self.get(key, fetch=True)

    def __setitem__(self, key, value):
        if not hasattr(self, 'save_data'):
            self.save_data = {}
        self.save_data[key] = value

    #def __delitem__(self, key):
    #   not implemented

    def __iter__(self):
        self.fetch()
        return iter(self.data)

    def __contains__(self, item):
        self.fetch()
        return item in self.data

    def _substitute(self, s, in_path=False, extra_dict=None):
        """Substitute all {key->key->key}. Returns KeyError if not
        successful. It does not fetch and is safe to use without a fetch."""
        def replace(match):
            parm = self
            for k in match.group(1).split('->'):
                if extra_dict and parm == self and k in extra_dict:
                    parm = extra_dict[k]
                else:
                    parm = parm.get(k)
                if parm is None:
                    raise KeyError("Cannot substitute '{}'".format(s))
            if in_path:
                return quote(str(parm), safe='')
            return str(parm)

        return self._parm_re.sub(replace, s)

    def _construct_path(self, path, extra_dict=None):
        if path is None:
            raise KeyError('Path not set')
        if path.startswith('+'):
            return self.path + self._substitute(path[1:], in_path=True, extra_dict=extra_dict)
        return self._substitute(path, in_path=True, extra_dict=extra_dict)

    def __getattr__(self, key):
        def args_to_path_parms(args):
            if len(var_parms) != len(args):
                raise TypeError('{}() takes {} positional argument(s) but {} were given'
                                .format(key, len(var_parms), len(args)))
            result = fixed_parms.copy()
            for v, a in zip(var_parms, args):
                result[v] = a
            return result

        method = False
        what = self._attrs.get(key)
        if not what:
            what = self._methods.get(key)
            method = True
        if not what:
            raise AttributeError("'{}' object has no attribute '{}'"
                                 .format(type(self).__name__, key))

        cls = what[0]
        is_list = isinstance(cls, list)
        if is_list:
            cls = cls[0]
        if isinstance(cls, str):
            cls = globals()[cls]

        has_path = hasattr(cls, '_path')
        if is_list or not has_path:
            path = what[1]
            what = what[2:]
        else:
            what = what[1:]

        var_parms = []
        fixed_parms = {}
        for p in what:
            pp = p.split('=', maxsplit=1)
            if len(pp) == 1:
                var_parms.append(p)
            else:
                fixed_parms[pp[0]] = self._substitute(pp[1])

        if method and is_list:
            result = lambda *args, **kwargs: List(cls, self.conn, self.provides,
                                                  self._construct_path(path, args_to_path_parms(args)),
                                                  kwargs)
        elif method and has_path:
            result = lambda *args, **kwargs: cls(self.conn, self.provides,
                                                 args_to_path_parms(args), kwargs)
        elif method:
            result = lambda *args, **kwargs: cls(self.conn, self.provides,
                                                 self._construct_path(path, args_to_path_parms(args)),
                                                 kwargs)
        elif has_path:
            result = cls(self.conn, self.provides, fixed_parms)
        else:
            path = self._construct_path(path, fixed_parms)
            result = cls(self.conn, self.provides, path)

        # cache the result in self.key
        setattr(self, key, result)
        return result

    def _get_provides(self):
        # let self.parms and self.data override the provided values
        result = { k: self.get(k) for k in self.provided }
        for k, v in self._provides.items():
            result[k] = self._substitute(v)
        return result
    provides = property(_get_provides)


def Str(conn, provided, path=None, get_parms=None, data=None):
    if data is None:
        data = conn.get(path, get_parms, raw=True).decode(errors='replace')
    return data


class Gitlab(BaseData):
    _path = ''
    _methods = {
        'group': ('Group', 'id'),
        'mrs': (['MR'], '/merge_requests'),
        'project': ('Project', 'id'),
        'todos': (['Todo'], '/todos'),
        'user': ('User', 'id'),
    }

    def __init__(self, host, personal_token, port=443, ca_certs=None):
        conn = GitlabConnection(host, personal_token, port=port, ca_certs=ca_certs)
        super().__init__(conn)
        self.me = self.conn.me


class User(BaseData):
    _path = '/users/{id}'

    def full_user(self):
        if self['public_email']:
            return '{} <{}>'.format(self['name'], self['public_email'])
        return self['name']


class Project(BaseData):
    _path = '/projects/{id}'
    _provides = { 'Project.id': '{id}' }
    _methods = {
        'commit': ('Commit', 'id'),
        'mr': ('MR', 'project_id={id}', 'iid'),
        'mrs': (['MR'], '+/merge_requests'),
    }


class MR(BaseData):
    _path = '/projects/{project_id}/merge_requests/{iid}'
    _provides = { 'Project.id': '{project_id}', 'MR.iid': '{iid}' }
    _attrs = {
        'approvals': ('Approvals',),
        'author': (User, 'id={author->id}'),
        'head_pipeline': ('Pipeline', 'id={head_pipeline->id}'),
        'notes': ('Notes',),
    }
    _methods = {
        'discussions': (['Thread'], '+/discussions'),
        'resource_label_events': (['LabelEvent'], '+/resource_label_events'),
    }

    def versions(self):
        """Returns a list of (version, base_commit_sha). Only the real
        changes made by a force push are returned."""
        result = []
        last = None
        for v in reversed(List(MRVersion, self.conn, self.provides, self.path + '/versions')):
            # GitLab includes also a change of the base sha (caused by
            # a dependency being merged) as a new version. Filter those out,
            # we're interested only in the versions where the head sha
            # changed.
            if last is not None and v['head_commit_sha'] == last['head_commit_sha']:
                result[-1][1] = v['base_commit_sha']
                continue
            result.append([v, v['base_commit_sha']])
            last = v
        assert result[-1][0]['head_commit_sha'] == self['diff_refs']['head_sha']
        return result

    def approve(self, sha):
        new_data = self.conn.post(self.path + '/approve', parms={ 'sha': sha })
        if hasattr(self, 'approvals'):
            self.approvals._update_data(new_data)

    def unapprove(self):
        new_data = self.conn.post(self.path + '/unapprove')
        if hasattr(self, 'approvals'):
            self.approvals._update_data(new_data)

    def assign(self, assign=True, user_id=None):
        """Assign or unassign (if assign is False) the MR to the given user.
        user_id=None means the current user."""
        if user_id is None:
            user_id = self.conn.me['id']
        # There's no API to assign or unassign. We always have to provide
        # the full list of MR assignees. To minimize race conditions, always
        # refetch the MR before setting the assignees.
        mr = self.copy()
        assignees = set(a['id'] for a in mr['assignees'])
        if assign:
            if user_id in assignees:
                # already there, no action
                return
            assignees.add(user_id)
        else:
            try:
                assignees.remove(user_id)
            except KeyError:
                # was not an assignee, no action
                return
        self['assignee_ids'] = list(assignees)
        self.save('put')

    def todo(self):
        self.conn.post(self.path + '/todo')


class Todo(BaseData):
    _path = '/todos/{id}'

    def mark_as_done(self):
        new_data = self.conn.post(self.path + '/mark_as_done')
        self._update_data(new_data)


class Group(BaseData):
    _path = '/groups/{id}'
    _methods = {
        'descendant_groups': (['Group'], '+/descendant_groups'),
        'mrs': ([MR], '+/merge_requests'),
    }


class MRVersion(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/versions/{id}'


class Commit(BaseData):
    _path = '/projects/{Project.id}/repository/commits/{id}'


class Thread(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/discussions/{id}'


class Notes(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/notes'


class LabelEvent(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/resource_label_events/{id}'


class Approvals(BaseData):
    _path = '/projects/{Project.id}/merge_requests/{MR.iid}/approvals'


class Pipeline(BaseData):
    _path = '/projects/{Project.id}/pipelines/{id}'
    _methods = {
        'bridges': (['BridgeJob'], '+/bridges'),
        'jobs': (['Job'], '+/jobs'),
    }


class BridgeJob(BaseData):
    # _path not set, the API cannot query this object on its own
    _attrs = {
        'downstream_pipeline': (Pipeline, 'Project.id={downstream_pipeline->project_id}',
                                'id={downstream_pipeline->id}'),
    }


class Job(BaseData):
    _path = '/projects/{Project.id}/jobs/{id}'
    _provides = { 'Project.id': '{pipeline->project_id}' }
    _attrs = {
        'trace': (Str, '+/trace'),
    }
    _methods = {
        'artifact': (Str, '+/artifacts/*{path}', 'path'),
    }
