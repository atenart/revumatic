import re
import socket
import urwid

from modules import (
    kerneloscope,
    packages,
)

DEFAULT_KERNELOSCOPE = 'kerneloscope.usersys.redhat.com'

class Kernel(packages.Package):
    paths = [
        # the ARK repo:
        'cki-project/kernel-ark',
        # the rest is fetched dynamically in apply_config
    ]
    hidden_paths = (
        '.*/kernel-test$',
    )
    marks = 1

    @classmethod
    def apply_config(cls, config, lab):
        # check for paths
        for group in lab.group('redhat').descendant_groups(search='kernel'):
            path = group['full_path']
            if 'sst' in path or 'tools' in path:
                continue
            # groups need to end with a slash
            cls.paths.append(path + '/')
        # check for Kerneloscope accessibility
        if 'rh_vpn' not in config['global']:
            server = config.get('kernel', 'kerneloscope',
                                fallback=DEFAULT_KERNELOSCOPE)
            try:
                socket.getaddrinfo(server, 80)
                # the Kerneloscope hostname is resolvable; assume RH VPN
                # access from now on, even on the next Revumatic runs
                config['global']['rh_vpn'] = 'yes'
                config.changed = True
            except socket.gaierror:
                pass

    def __init__(self, config):
        self.kscope = None
        if config.getboolean('global', 'rh_vpn', fallback=False):
            server = config.get('kernel', 'kerneloscope',
                                fallback=DEFAULT_KERNELOSCOPE)
            self.kscope = kerneloscope.Kerneloscope(server, error_notify=self.kscope_error)

        subsystems = config.get('kernel', 'subsystem', fallback='').lower()
        self.subsystems = subsystems.split() if subsystems else ()
        self.review_labels = []
        self.review_labels_fallback = []
        need_fallback = True
        for subs in self.subsystems or ('all',):
            if subs == 'all':
                self.review_labels.append('acks::needsreview')
                need_fallback = False
            else:
                self.review_labels.append('acks::{}::needsreview'.format(subs))
        if need_fallback:
            self.review_labels_fallback.append('acks::needsreview')

        self._bugzilla_re = re.compile(r'^Bugzilla: (https?://bugzilla\.redhat\.com/.*)$',
                                       re.MULTILINE)
        self._upstream_re = re.compile(r'^commit ([0-9a-f]{40})( \(.*\))?$|^\(cherry picked from commit ([0-9a-f]{40})\)$',
                                       re.MULTILINE)
        self._rhel_only_re = re.compile(r'RHEL[ _-]?[0-9]*([0-9]\.[0-9]+)?[ _-]only')
        self._omit_re = re.compile(r'^Omitted-fix: ([0-9a-f]{8,40})( .*)?$', re.MULTILINE)
        self._kabi_re = re.compile('RH_KABI_|__GENKSYMS__')
        self._nack_re = re.compile(r'^ *Nacked-by: ', re.MULTILINE)

        self._cmd_re = re.compile(r'^request-([a-z-]*-)?evaluation$')

    def kscope_error(self, error):
        urwid.emit_signal(self, 'status-changed')

    def get_status(self):
        return ('header-problem', 'K✘') if self.kscope and self.kscope.error else ''

    def get_extra_help(self):
        if self.kscope and self.kscope.error:
            return [('title', 'Error status:\n\n'),
                    ('list-problem', 'K✘'), '  Kerneloscope: {}\n'.format(self.kscope.error)
                   ]
        return None

    def is_hidden(self, path):
        for hidden in self.hidden_paths:
            if re.match(hidden, path):
                return True
        return False

    def parse_dep_label(self, label):
        dep = label.split('::')[-1]
        if dep == 'OK':
            return None
        return dep

    def get_base_sha(self, mr):
        for label in mr['labels']:
            if label.startswith('Dependencies::'):
                return self.parse_dep_label(label)
        return None

    def get_historical_base_sha(self, version, activity):
        deps = None
        for _, act_type, content in activity:
            if act_type == 'version' and content > version:
                break
            if (act_type == 'label' and content['action'] == 'add' and
                content['label'] and content['label']['name'].startswith('Dependencies::')):
                deps = self.parse_dep_label(content['label']['name'])
        return deps

    def get_bugzilla(self, mr):
        return [match.group(1).rstrip()
                for match in self._bugzilla_re.finditer(mr['description'])]

    def get_upstream(self, commit):
        matches = []
        for match in self._upstream_re.finditer(commit.message):
            if match.group(1) is None:
                matches.append(match.group(3))
            else:
                matches.append(match.group(1))
        if matches:
            return matches
        match = self._rhel_only_re.search(commit.message)
        if match is None:
            return []
        return None

    def get_kabi_mark(self, diff):
        for f in diff:
            for h in f:
                if (self._kabi_re.search(h.lines_added) or
                    self._kabi_re.search(h.lines_removed)):
                    return True
        return False

    def get_marks(self, commit, diff):
        kabi_mark = 'K' if self.get_kabi_mark(diff) else ' '
        return (kabi_mark,)

    def get_marks_help(self):
        return (('K', 'kABI modifications', 'err'),)

    def get_label_color(self, label):
        cls = ''
        components = label.split('::')
        if components[0] == 'CKI_RT':
            return cls
        for c in components[1:]:
            if c == 'OK':
                cls = 'ok'
            elif c.startswith('Needs'):
                cls = 'warn'
            elif c.startswith('NACK') or c.endswith('Failed') or c.endswith('Blocked'):
                cls = 'err'
        return cls

    def filter_todo_labels(self, labels):
        result = []
        result_fallback = []
        result_additional = []
        for label in labels:
            lower_label = label.lower()
            if lower_label in self.review_labels:
                result.append(label)
            elif lower_label in self.review_labels_fallback:
                result_fallback.append(label)
            elif lower_label == 'acks::nacked':
                result.append(label)
            elif lower_label.startswith('cki::failed'):
                result_additional.append(label)
        result = result or result_fallback
        result.extend(result_additional)
        return result

    def get_diff(self, sha):
        if not self.kscope:
            return None
        return self.kscope.diff(sha)

    def get_fixes(self, commit_list, mr, repo):
        if not self.kscope:
            return ()
        return self.kscope.fixes(commit_list, repo.remote.url, mr['target_branch'])

    def exclude_fixes(self, commits, mr):
        result = [match.group(1) for match in self._omit_re.finditer(mr['description'])]
        for c in commits:
            result.extend(match.group(1) for match in self._omit_re.finditer(c.message))
        return result

    def get_edit_templates(self, lab):
        # nacking is possible only with a public email set
        templates = []
        if lab.me['public_email']:
            full_user = lab.me.full_user()
            templates.append(('Nacked-by: {}\n'.format(full_user),
                              'Nack'))
            templates.append(('Rescind-nacked-by: {}\n'.format(full_user),
                              'Rescind a previous nack'))
        return templates

    def postformat_comment(self, comment, for_commit):
        if not for_commit:
            return comment
        if not self._nack_re.search(comment):
            return comment
        result = []
        nacks = []
        skip_empty = False
        for line in comment.split('\n'):
            if self._nack_re.match(line):
                nacks.append(line.lstrip())
                skip_empty = True
                continue
            if line.strip() == '' and skip_empty:
                continue
            skip_empty = False
            result.append(line)
        if result and result[-1].strip() != '':
            result.append('')
        result.extend(nacks)
        return '\n'.join(result)

    def filter_note(self, note, new_thread):
        if note['system']:
            if (note['body'].startswith('approved this merge request') or
                note['body'].startswith('unapproved this merge request') or
                note['body'].startswith('marked this merge request as')):
                return True
            return False
        if note['author']['username'] in ('cki-kwf-bot', 'cki-admin-bot'):
            return False
        if (note['body'].startswith('Passed lnst testing') or
            note['body'].startswith('Skipping lnst testing')):
            return False
        if self._cmd_re.match(note['body']):
            return False
        return True


packages.add(Kernel)
