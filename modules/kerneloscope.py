import json
import urllib.request
import weakref

class KerneloscopeError(Exception):
    pass

class KerneloscopeResult(KerneloscopeError):
    pass

class Kerneloscope:
    def __init__(self, server, error_notify=None):
        self.error = None
        self.error_notify = None
        if error_notify:
            self.error_notify = weakref.WeakMethod(error_notify)
        self.enabled = bool(server)
        if not server.endswith('/'):
            server += '/'
        if not server.startswith('http:') and not server.startswith('https:'):
            server = 'http://' + server
        self._url = server + 'rpc/'

    def _call(self, name, **kwargs):
        if not self.enabled:
            return None
        data = json.dumps(kwargs).encode('utf-8')
        req = urllib.request.Request('{}{}/'.format(self._url, name),
                                     data=data, method='POST')
        req.add_header('Content-Type', 'application/json')
        try:
            resp = urllib.request.urlopen(req)
        except (urllib.error.HTTPError, urllib.error.URLError) as e:
            raise KerneloscopeError('Connection error {}'.format(str(e)))

        resp = json.loads(resp.read().decode('utf-8'))
        if 'error' in resp:
            raise KerneloscopeResult('Server returned error {}'.format(resp['error']))
        if 'result' not in resp:
            raise KerneloscopeError('Unknown server response')
        return resp['result']

    def call(self, name, **kwargs):
        try:
            return self._call(name, **kwargs)
        except KerneloscopeResult:
            return None
        except KerneloscopeError as e:
            self.error = str(e)
            notify = self.error_notify() if self.error_notify else None
            if notify:
                notify(self.error)
            return None

    def diff(self, commit):
        return self.call('get_diff', commit=commit)

    def fixes(self, upstream_list, repo_url, branch):
        resp = self.call('get_missing_fixes', commit_with_subject=True, commits=upstream_list,
                         tree=['try_url', '{}#{}'.format(repo_url, branch)])
        if not resp:
            return ()
        subjects = {}
        for r in resp:
            subjects[r['commit']] = r['subject']
        result = []
        for r in resp:
            if r['added']:
                continue
            if 'fixes' in r and r['fixes']:
                result.extend((r['commit'], c, subjects[c]) for c in r['fixes'])
        return result
