from modules import compat


class UnknownAction(Exception):
    pass


display_keys = { 'up': '↑', 'down': '↓', 'left': '←', 'right': '→',
                 'ctrl up': 'ctrl↑', 'ctrl down': 'ctrl↓',
                 'ctrl left': 'ctrl←', 'ctrl right': 'ctrl→',
                 ' ': 'space', '<0>': 'ctrl space',
               }

# translation of keys that cannot be saved in the config file to something
# that can be
config_keys = { ' ': 'space' }


key_aliases = {
    'confirm_find_file': 'activate',
    'confirm_template': 'activate',
    'open': 'activate',
}


class Keys:
    # merge request
    approve = 'A'
    assign = '+'
    bugzilla = 'b'
    ci_status = '?'
    comment = 'c'
    compare_versions = 'v'
    compare_last_approved_version = 'V'
    diff_next = 'd'
    diff_prev = 'D'
    find_file_next = 'l'
    find_file_prev = 'L'
    from_content = 'enter'
    full_activity = 'a'
    fullscreen = 'f'
    gitlab = 'g'
    insert_template = 'ctrl t'
    interdiff = 'u'
    interdiff_alt = 'U'
    search = '/'
    search_next = 'n'
    submit_comments = 'C'
    to_commits = ('esc', 'q')
    to_content = 'enter'
    todo_add = 't'
    unapprove = 'X'
    unassign = '-'

    # todo and other MR lists
    all_mrs = 'a'
    mark_done = 'D'
    refresh = 'r'

    # key config
    remove = 'delete'

    # general
    focus_next = 'tab'
    focus_prev = 'shift tab'
    activate = 'enter'
    next_item = ('ctrl down', 'j')
    prev_item = ('ctrl up', 'k')
    help = ('f1', 'meta h')
    quit = 'q'

    # cursor movement
    cursor_up = 'up'
    cursor_down = 'down'
    cursor_left = 'left'
    cursor_right = 'right'
    cursor_max_left = 'home'
    cursor_max_right = 'end'
    cursor_page_up = 'page up'
    cursor_page_down = 'page down'
    cursor_top = 'ctrl home'
    cursor_bottom = 'ctrl end'
    redraw_screen = 'ctrl l'

    # editor
    delete_line = 'ctrl d'
    paste = 'ctrl v'
    undo = 'ctrl z'
    redo = 'ctrl r'
    select = '<0>' # ctrl space

    def __init__(self, config):
        self._modified = set()
        rev_config_keys = { v: k for k, v in config_keys.items() }
        for name, key in config.items():
            attr_name = name.replace('-', '_')
            attr_name = compat.compat_actions.get(attr_name, attr_name)
            try:
                getattr(self, attr_name)
            except AttributeError:
                raise UnknownAction('Unknown action \'{}\' in the config file.'.format(name))
            value = tuple(rev_config_keys.get(v, v) for v in key.split('\n'))
            setattr(self, attr_name, value)
            self._modified.add(attr_name)

    def __getitem__(self, name):
        name = name.replace('-', '_')
        key = getattr(self, key_aliases.get(name, name))
        if type(key) == str:
            return (key,)
        return key

    def __setitem__(self, name, value):
        name = name.replace('-', '_')
        if name in self._modified:
            key = getattr(self, name)
            if type(key) == str:
                key = (key,)
            if value in key:
                return
        else:
            key = ()
            self._modified.add(name)
        new_value = key + (value,)
        if len(new_value) == 1:
            new_value = new_value[0]
        setattr(self, name, new_value)

    def __delitem__(self, name):
        name = name.replace('-', '_')
        try:
            self._modified.remove(name)
        except KeyError:
            return
        setattr(self, name, getattr(Keys, name))
