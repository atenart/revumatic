import re
import urwid

from modules import (
    git,
    gitlab,
    mr,
    packages,
    screen,
    ui,
    utils,
)

class BaseScreen(screen.BaseScreen):
    name = ''
    config_section = None
    default_sort = None
    display_date = 'mr_created'

    @classmethod
    def add_arguments(cls, parser):
        super().add_arguments(parser)
        parser.add_argument('-A', '--hide-approved', action='store_true',
                            help='hide the merge requests approved by you')

    def __init__(self, app, args):
        super().__init__(app, args)
        self.hide_approved = args.hide_approved

    def add_actions(self, actions):
        return

    def init_ui(self):
        self.widgets.mr_list = ui.TraversableListBox()

        actions = [
            { 'name': 'open', 'prio': 3, 'help': 'open' },
            { 'name': 'gitlab', 'prio': 3, 'help': 'open in GitLab' },
            { 'name': 'all-mrs', 'prio': 2, 'help': 'all MRs',
              'long_help': 'show all/show supported merge requests' },
            { 'name': 'refresh', 'prio': 2, 'help': 'refresh',
              'confirm': 'Do you want to refresh?' },
            { 'name': 'quit', 'prio': 0, 'help': self.app.get_quit_text(),
              'confirm': 'Do you want to quit?' },
            { 'name': 'next-item', 'prio': 1, 'long_help': 'next item' },
            { 'name': 'prev-item', 'prio': 1, 'long_help': 'previous item' },
        ]
        self.add_actions(actions)

        self.action_tracker.add_actions(self.widgets.mr_list, actions)

        return self.widgets.mr_list

    def load_data(self):
        def create_walker(mrs):
            if mrs is None:
                return None
            return ui.PrettyListWalker((self.format_item(item) for item in mrs),
                                       self.app.focus_map, self.app.passive_map)

        popup = self.app.wait_popup('Loading the {} list...'.format(self.name))
        # get my approvals
        self.mrs_approved = set(mr['id'] for mr in self.app.lab.mrs(
                view='simple', scope='all', state='opened',
                approved_by_ids=[self.app.lab.me['id']]))
        # get the actual list
        self.mrs_known = []
        self.mrs_all = []
        self.mapping_known = []
        self.mapping_all = []
        self.fetch_list()
        if len(self.mrs_known) == len(self.mrs_all):
            # no hidden MRs
            self.mrs_all = None
        self.action_tracker.set_action_enabled('all-mrs', bool(self.mrs_all))
        self.sort_list()
        popup.stop()

        self.walker_known = create_walker(self.mrs_known)
        self.walker_all = create_walker(self.mrs_all)
        self.set_list(self.walker_known, self.mrs_known)

    def sort_list(self):
        sort_key = self.app.config.get(self.config_section, 'sort-by',
                                       fallback=self.default_sort)
        if not sort_key:
            return

        reverse = False
        if 'reverse' in sort_key:
            sort_key = sort_key.replace('reverse', '').strip()
            reverse = True

        try:
            self.mrs_known.sort(key=lambda todo: todo[sort_key], reverse=reverse)
            if self.mrs_all:
                self.mrs_all.sort(key=lambda todo: todo[sort_key], reverse=reverse)
        except KeyError:
            raise screen.ScreenError('Invalid sort key specified in the config file')

    def fetch_list(self):
        raise NotImplementedError

    def parse_mr(self, pkg, obj, data, project_name):
        """Parse and return the given merge request. It will be added to the
        internal list of MRs to display. If 'pkg' is None, the MR will be
        hidden by default and an action to show/hide also the hidden MRs
        will be added."""
        approved = data['id'] in self.mrs_approved

        flags = []
        if data['state'] != 'opened':
            flags.append(data['state'])
        if data['work_in_progress']:
            flags.append('draft')

        my_id = self.app.lab.me['id']
        info_flags = []
        if any(u['id'] == my_id for u in data['assignees']):
            info_flags.append('assigned')
        if approved:
            info_flags.append('approved')

        labels = []
        if pkg:
            for label in pkg.filter_todo_labels(data['labels']):
                color = pkg.get_label_color(label)
                if color is None:
                    continue
                labels.append((color, label))

        mr = { 'obj': obj,
               'url': data['web_url'],
               'mr_created': gitlab.parse_datetime(data["created_at"]),
               'mr_updated': gitlab.parse_datetime(data["created_at"]),
               'mr_id': data['iid'],
               'branch': '{}/{}'.format(project_name, data['target_branch']),
               'title': utils.oneline(data['title']),
               'author': data['author']['name'],
               'flags': flags,
               'info_flags': info_flags,
               'labels': labels,
             }
        if pkg and not (approved and self.hide_approved):
            self.mrs_known.append(mr)
            self.mapping_known.append(len(self.mrs_all))
        self.mrs_all.append(mr)
        self.mapping_all.append(max(len(self.mrs_known) - 1, 0))
        return mr

    def format_item(self, todo):
        result = []
        result.append(('list-date', '{:%Y-%m-%d}'.format(todo[self.display_date])))
        result.append(('list', ' '))
        if todo.get('who'):
            result.append(('list-person', todo['who']))
            result.append(('list', ' '))
        if todo.get('action'):
            result.append(('list', '{} '.format(todo['action'])))
        result.append(('list-sha', todo['branch']))
        result.append(('list-id', '!{}'.format(todo['mr_id'])))
        if todo['author']:
            result.append(('list', ' '))
            result.append(('list-person', todo['author']))
        result.append(('list', ' {}'.format(todo['title'])))
        line2 = []
        if todo['flags']:
            line2.append(('list-problem', ' '.join(todo['flags'])))
        if todo['info_flags']:
            if line2:
                line2.append(('list', ' '))
            line2.append(('list-info', ' '.join(todo['info_flags'])))
        for color, label in todo['labels']:
            if line2:
                line2.append(('list', ' '))
            line2.append(('list-label{}'.format('-' + color if color else ''), label))
        if todo.get('body'):
            if line2:
                line2.append(('list', ' '))
            line2.append(('list-comment', todo['body']))
        result.append(('list', '\n           '))
        result.extend(line2)
        return result

    def update_title(self):
        title = [
            ('header', ' GitLab {} list '.format(self.name)),
            ('header-commits', ' {} items '.format(len(self.mrs))),
            ('header', ' {} '.format(self.app.lab.me.full_user())),
        ]
        self.app.set_header(title)

    def update_actions(self):
        self.action_tracker.set_action_enabled('open', bool(self.mrs))
        self.action_tracker.set_action_enabled('gitlab', bool(self.mrs))
        self.app.update_help()

    def set_list(self, walker, mrs):
        self.widgets.mr_list.body = walker
        self.mrs = mrs
        self.update_actions()
        self.update_title()

    def switch_list(self):
        try:
            # Has to be read before self.set_list()
            index = self.widgets.mr_list.focus_position
        except IndexError:
            index = 0

        if self.mrs == self.mrs_known:
            self.set_list(self.walker_all, self.mrs_all)
            mapping = self.mapping_known
        else:
            self.set_list(self.walker_known, self.mrs_known)
            mapping = self.mapping_all

        try:
            # Has to be set after self.set_list()
            self.widgets.mr_list.focus_position = mapping[index]
        except IndexError:
            pass

    def del_mr(self, index):
        def update_mapping(index, m, other):
            del m[index]
            for i, v in enumerate(other):
                if v >= index:
                    other[i] -= 1

        if self.mrs == self.mrs_known:
            m1 = self.mapping_known
            m2 = self.mapping_all
            o_mrs = self.mrs_all
            o_walker = self.walker_all
        else:
            m1 = self.mapping_all
            m2 = self.mapping_known
            o_mrs = self.mrs_known
            o_walker = self.walker_known
        update_mapping(index, m1, m2)

        if o_mrs:
            # delete the MR also from the other list
            obj = self.mrs[index]['obj']
            for j, mr in enumerate(o_mrs):
                if mr['obj'] == obj:
                    del o_mrs[j]
                    del o_walker[j]
                    update_mapping(j, m2, m1)
                    break

        del self.mrs[index]
        del self.widgets.mr_list.body[index]
        self.update_title()

    def open_mr(self, browser=False):
        url = self.mrs[self.widgets.mr_list.focus_position]['url']
        if browser:
            self.app.web_browser(url)
        else:
            self.app.start_screen(mr.Screen, mr_id=url)

    def scroll(self, amount):
        try:
            self.widgets.mr_list.focus_position += amount
        except IndexError:
            pass

    def action(self, what):
        if what == 'quit':
            self.app.quit()
        elif what == 'next-item':
            self.scroll(1)
        elif what == 'prev-item':
            self.scroll(-1)
        elif what == 'refresh':
            self.load_data()
        elif what == 'open':
            self.open_mr()
        elif what == 'gitlab':
            self.open_mr(browser=True)
        elif what == 'all-mrs':
            self.switch_list()
        else:
            return False
        return True
