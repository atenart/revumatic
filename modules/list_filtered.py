import re

from modules import (
    filter_mr,
    list_base,
    packages,
    screen,
)

class Screen(list_base.BaseScreen):
    command = 'list'
    help = 'show a filtered list of merge requests'
    help_epilog = '''
Filter is a list of labels. Separate the labels by '+' for AND, by a whitespace
for OR and by '-' for AND NOT. Alternatively, you may use the logical operators
'&', '|' and '~' in their usual meaning.
Parenthesis and shell-style wildcards are recognized.

Examples:
Subsystem:net Subsystem:mm      matches both net and mm subsystems.
Subsystem:net + *Failed*        matches net subsystem with something failed.
Subsystem:mm - *Failed*         matches mm subsystem with nothing failed.
-Subsystem:net -Subsystem:mm    everything except the net and mm subsystems.

In addition, certain keywords are recognized in place of the labels. Currently,
those are: {keywords}.

Example:
*NeedsReview - draft            everything needing review except for drafts.

Hint: separate your expression from the previous arguments by '--'. That way,
you can safely start the expression with '-'.
'''.format(keywords=', '.join(sorted(filter_mr.FilterParser.keywords.keys())))
    help_raw = True

    name = 'filtered'
    config_section = 'list'
    default_sort = 'reverse mr_created'

    @classmethod
    def add_arguments(cls, parser):
        super().add_arguments(parser)
        parser.add_argument('filter', nargs='*', help='filter to use')

    def __init__(self, app, args):
        super().__init__(app, args)
        try:
            self.filter = filter_mr.FilterParser(' '.join(args.filter))
        except filter_mr.ParserError as e:
            raise screen.ScreenError(e)

    def fetch_list(self):
        for path in packages.get_paths():
            if path.endswith('/'):
                new_mrs = self.app.lab.group(path[:-1]).mrs(state='opened')
            else:
                new_mrs = self.app.lab.project(path).mrs(state='opened')
            for mr in new_mrs:
                if not self.filter(mr):
                    continue

                path = mr['references']['full'].split('!')[0]
                pkg = packages.find(path, exclude_hidden=True)
                if not pkg:
                    continue

                self.parse_mr(pkg, mr,  mr, path.split('/')[-1])
