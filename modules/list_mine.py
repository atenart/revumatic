from modules import (
    git,
    list_base,
    packages,
)

class Screen(list_base.BaseScreen):
    command = 'mine'
    help = 'show the list of your merge requests'
    help_epilog = '''The --assignee, --author and --reviewer options can be
                  combined. If neither is specified, --reviewer is assumed.'''

    name = 'my MR'
    config_section = 'mine'
    default_sort = 'reverse mr_created'

    @classmethod
    def add_arguments(cls, parser):
        super().add_arguments(parser)
        parser.add_argument('--assignee', action='store_true',
                            help='show the merge requests assigned to you')
        parser.add_argument('--author', action='store_true',
                            help='show the merge requests created by you')
        parser.add_argument('--reviewer', action='store_true',
                            help='show the merge requests having you as a reviewer')
        parser.set_defaults(cls_kwargs=('author', 'reviewer', 'assignee'))

    def __init__(self, app, args):
        super().__init__(app, args)
        self.which = [k for k in ('assignee', 'author', 'reviewer')
                        if getattr(args, k, False)]
        if not self.which:
            self.which = ['reviewer']

    def fetch_list(self):
        seen = set()
        for w in self.which:
            parms = { w + '_id': self.app.lab.me['id'], 'scope': 'all', 'state': 'opened' }
            data = self.app.lab.mrs(**parms)
            for mr in data:
                if mr['id'] in seen:
                    continue
                seen.add(mr['id'])

                path = mr['references']['full'].split('!')[0]
                pkg = packages.find(path, exclude_hidden=(w != 'author'))
                if not pkg:
                    continue

                self.parse_mr(pkg, mr, mr, path.split('/')[-1])
