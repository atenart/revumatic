import re
import urwid

from modules import (
    git,
    gitlab,
    list_base,
    mr,
    packages,
    screen,
    ui,
)

class Screen(list_base.BaseScreen):
    command = 'todo'
    help = 'show the TODO list'

    name = 'TODO'
    config_section = 'todo'
    default_sort = 'reverse created'
    display_date = 'created'

    def add_actions(self, actions):
        actions.append(
            { 'name': 'mark-done', 'prio': 3, 'help': 'mark as done',
              'confirm': 'Do you want to mark this item as done?' })

    def fetch_list(self):
        todos = self.app.lab.todos(type='MergeRequest')

        nl_re = re.compile('\n+')
        for t in todos:
            pkg = packages.find(t['project']['path_with_namespace'], exclude_hidden=True)
            mr = self.parse_mr(pkg, t, t['target'], t['project']['name'])

            if t['action_name'] in ('directly_addressed', 'mentioned'):
                # these two actions are the same thing, see
                # https://gitlab.com/gitlab-org/gitlab/-/issues/119232
                mr['who'] = t['author']['name']
                mr['action'] = 'mentioned you on'
                mr['body'] = nl_re.sub(' ', t['body'])
                mr['author'] = None
            elif t['action_name'] == 'review_requested':
                mr['action'] = 'review of'
            elif t['action_name'] == 'approval_required':
                mr['action'] = 'requested approval of'
            elif t['action_name'] == 'assigned':
                mr['who'] = t['author']['name']
                mr['action'] = 'assigned you'
            elif t['action_name'] == 'build_failed':
                mr['action'] = 'failed build for'
            elif t['action_name'] == 'marked':
                mr['action'] = 'todo'
            else:
                # fallback for unknown actions
                mr['action'] = t['action_name']

            mr['created'] = gitlab.parse_datetime(t['created_at'])

    def update_actions(self):
        self.action_tracker.set_action_enabled('mark-done', bool(self.mrs))
        super().update_actions()

    def mark_done(self):
        index = self.widgets.mr_list.focus_position
        todo = self.mrs[index]
        popup = self.app.wait_popup('Working...')
        todo['obj'].mark_as_done()
        popup.stop()
        self.del_mr(index)
        self.update_actions()

    def action(self, what):
        if super().action(what):
            return
        if what == 'mark-done':
            self.mark_done()
