import functools
import itertools
import os
import re
import subprocess
import urllib.parse
import urwid

from modules import (
    formatters,
    git,
    gitlab,
    mr_ci,
    mr_commits,
    mr_layout,
    patch,
    screen,
    ui,
    ui_markup,
    utils,
)


class CommentStorage:
    GENERAL = 'general'

    def __init__(self, config_dir, repo, iid):
        self.config_dir = config_dir
        self.subpath = os.path.join('comments',
                                    repo.project.replace('/', '-'), str(iid))
        self.path = os.path.join(config_dir, self.subpath)
        self.has_dir = os.path.exists(self.path)

    def __getitem__(self, key):
        try:
            with open(os.path.join(self.path, key), 'r') as f:
                return f.read().split('\n', maxsplit=2)
        except FileNotFoundError:
            raise KeyError('config entry {} not present'.format(key))

    def __setitem__(self, key, value):
        name, upstream, content = value
        if not content:
            self.__delitem__(key)
            return
        self.create_dir()
        path = os.path.join(self.path, key)
        with open(path + '.new', 'w') as f:
            f.write('{}\n{}\n{}'.format(name, upstream, content))
        os.rename(path + '.new', path)

    def __delitem__(self, key):
        try:
            os.remove(os.path.join(self.path, key))
        except FileNotFoundError:
            return
        self.cleanup()

    def __iter__(self):
        if not self.has_dir:
            return
        files = os.listdir(self.path)
        # return the general comment first
        if self.GENERAL in files:
            yield self.GENERAL
        for name in files:
            if name == self.GENERAL:
                continue
            yield name

    def create_dir(self):
        if self.has_dir:
            return
        os.makedirs(self.path)
        self.has_dir = True

    def cleanup(self):
        if not self.has_dir:
            return
        prev = os.getcwd()
        os.chdir(self.config_dir)
        try:
            os.removedirs(self.subpath)
        except OSError:
            pass
        os.chdir(prev)
        self.has_dir = os.path.exists(self.path)


class LayoutError(Exception):
    pass


class Screen(screen.BaseScreen):
    def __init__(self, app, args):
        super().__init__(app, args)
        mr_id = args.mr_id
        self.project_path = None
        if mr_id.startswith('http'):
            mr_id = mr_id.rstrip('/')
            url = urllib.parse.urlsplit(mr_id)
            if (url.scheme not in ('http', 'https') or url.query):
                raise screen.ScreenError('Invalid merge request URL: {}'.format(mr_id))
            if url.netloc != self.app.config['global']['gitlab']:
                raise screen.ScreenError('The server in the URL does not match the server in the config.')
            path = url.path
            if path.startswith('/'):
                path = path[1:]
            components = path.rsplit('/', maxsplit=3)
            if (len(components) != 4 or components[1] != '-' or
                    components[2] != 'merge_requests'):
                raise screen.ScreenError('Invalid merge request URL: {}'.format(mr_id))
            self.project_path = components[0]
            mr_id = components[3]
        try:
            self.mr_id = int(mr_id)
        except ValueError:
            raise screen.ScreenError('The merge request parameter should be a number or an URL.')

    def load_data(self):
        paths = self.app.get_git_paths()
        if not self.project_path:
            paths = ['.'] + paths
        self.repos = git.RepoCollection(paths, self.app.config['global']['gitlab'])
        self.check_repo()

    def check_repo(self, **kwargs):
        try:
            self.repo, self.pkg = self.repos.find_repo(self.project_path)
        except git.GitConfigError as e:
            self.repo_retry(e, self.check_repo, start=True)
            return
        except git.GitBadProject as e:
            self.app.message_popup(self.quit, str(e))
            return
        if not self.repo:
            self.app.edit_popup(self.add_repo,
                'Enter the local file system path to a repository for {}:'.format(self.project_path),
                allow_esc=True)
            return

        self.adjust_ui()

        self.project = self.app.lab.project(self.repo.project)
        self.load_mr()

    def add_repo(self, text=None):
        path = text
        if path is None:
            self.quit()
            return
        assert self.project_path
        try:
            self.repos.add_repo(path, self.project_path)
        except git.GitConfigError as e:
            cb = functools.partial(self.add_repo, path)
            self.repo_retry(e, cb, start=True)
            return
        except git.GitError as e:
            self.app.message_popup(self.check_repo, str(e))
            return
        self.app.add_git_path(path)
        self.check_repo()

    def repo_retry(self, exc, callback, button=None, start=False):
        if start:
            self.app.buttons_popup(self.repo_retry, str(exc),
                                   ('~dismiss', '~fix it for me'),
                                   allow_esc=True,
                                   callback_args=(exc, callback))
            return
        if button and button.startswith('fix'):
            subprocess.run(exc.suggested_command, check=True)
            self.app.redraw_screen()
            callback()
            return
        self.quit()

    def init_ui(self):
        self.widgets.commit_list = ui.TraversableListBox()
        coupler = ui.Coupler()
        self.widgets.content_backport = ui.CoupledViewer(coupler)
        self.widgets.content_upstream = ui.CoupledViewer(coupler)

        self.widgets.comment_header = urwid.Text('', wrap='ellipsis')
        self.widgets.comment = ui.BoxedEdit()
        self.widgets.comment_wrapper = urwid.Pile((
            ('pack', urwid.AttrMap(self.widgets.comment_header, 'subheader')),
            self.widgets.comment))

        cfg_layout_name = self.app.config.get('mr', 'layout', fallback='standard')
        layout_name = (re.sub('-.', lambda s: s[1].upper(), cfg_layout_name).capitalize()
                       + 'Layout')
        try:
            layout_class = getattr(mr_layout, layout_name)
        except AttributeError:
            raise screen.ScreenError('Layout \'{}\' not found.'.format(cfg_layout_name))

        comp1 = self.widgets.content_backport
        comp2 = self.widgets.content_upstream
        if self.app.config.getboolean('mr', 'swap-compare', fallback=False):
            comp1, comp2 = comp2, comp1
        self.layout = layout_class(self.app.workplace,
                                   self.widgets.commit_list,
                                   comp1,
                                   comp2,
                                   self.widgets.comment_wrapper)

        self.widgets.content = self.layout.create_content_widgets()
        for w, _ in self.widgets.content:
            if not w.readonly:
                raise LayoutError('Layout \'{}\' is buggy - content view must be read only.'
                                  .format(cfg_layout_name))

        interdiff_hlp = 'show the difference between the commit and upstream or between the commit versions'
        common_actions = [
            { 'name': 'comment', 'prio': 3, 'help': 'add/edit comment' },
            { 'name': 'interdiff', 'prio': 3, 'help': 'interdiff',
              'long_help': interdiff_hlp },
            { 'name': 'full-activity', 'prio': 2, 'help': 'all activity',
              'long_help': 'show/hide full merge request activity' },
            { 'name': 'next-item', 'prio': 1, 'help': 'next', 'long_help': 'next commit' },
            { 'name': 'prev-item', 'prio': 1, 'long_help': 'previous commit' },
        ]
        if self.layout.alternative_compare:
            common_actions.append({ 'name': 'interdiff-alt', 'prio': 3,
                                    'long_help': '{} ({})'.format(interdiff_hlp, self.layout.alternative_compare) })
        commit_list_actions = [
            { 'name': 'bugzilla', 'prio': 4, 'help': 'open Bugzilla' },
            { 'name': 'gitlab', 'prio': 4, 'help': 'open in GitLab' },
            { 'name': 'compare-versions', 'prio': 4, 'help': 'compare versions' },
            { 'name': 'compare-last-approved-version', 'prio': 4,
              'long_help': 'compare with the last approved version' },
            { 'name': 'submit-comments', 'prio': 4, 'help': 'submit comments' },
            { 'name': 'approve', 'prio': 4, 'help': 'approve',
              'confirm': 'Do you want to approve the merge request?' },
            { 'name': 'unapprove', 'prio': 4, 'help': 'unapprove',
              'confirm': 'Do you want to unapprove the merge request?' },
            { 'name': 'assign', 'prio': 4, 'help': 'assign to me',
              'confirm': 'Do you want to assign the merge request to yourself?' },
            { 'name': 'unassign', 'prio': 4, 'help': 'unassign me',
              'confirm': 'Do you want to unassign yourself from the merge request?' },
            { 'name': 'todo-add', 'prio': 4, 'help': 'todo',
              'long_help': 'add this merge request to the TODO list',
              'confirm': 'Do you want to add this merge request to your TODO list?' },
            { 'name': 'ci-status', 'prio': 4, 'help': 'CI status',
              'long_help': 'show CI status of the merge request' },
            { 'name': 'find-file-next', 'prio': 4, 'help': 'find file',
               'long_help': 'find the next commit touching a file' },
            { 'name': 'find-file-prev', 'prio': 4,
               'long_help': 'find the previous commit touching a file' },
            { 'name': 'to-content', 'prio': 4 },
            { 'name': 'quit', 'prio': 0, 'help': self.app.get_quit_text(),
              'confirm': 'Do you want to quit this merge request?' },
        ] + common_actions
        content_actions = [
            { 'name': 'to-commits', 'prio': 10, 'help': 'return',
              'long_help': 'return to the commit list' },
            { 'name': 'fullscreen', 'prio': 2, 'help': 'fullscreen',
              'long_help': 'switch fullscreen' },
            { 'name': 'search', 'prio': 2, 'help': 'search' },
            { 'name': 'search-next', 'prio': 2, 'help': 'search next' },
        ] + common_actions
        if self.layout.content_enter_supported:
            content_actions.append({ 'name': 'from-content', 'prio': 4 })
        content_diff_actions = content_actions + [
            { 'name': 'diff-next', 'prio': 2, 'help': 'next difference',
              'long_help': 'jump to the next difference' },
            { 'name': 'diff-prev', 'prio': 2,
              'long_help': 'jump to the previous difference' },
        ]
        comment_actions = [
            { 'name': 'to-commits', 'prio': 10, 'help': 'return',
              'long_help': 'return to the commit list' },
            { 'name': 'insert-template', 'prio': 4, 'help': 'tag',
              'long_help': 'insert a tag/template' },
            { 'name': 'next-item', 'prio': 1, 'help': 'next', 'long_help': 'next commit' },
            { 'name': 'prev-item', 'prio': 1, 'long_help': 'previous commit' },
        ]

        self.action_tracker.add_actions(self.widgets.commit_list, commit_list_actions)
        self.action_tracker.add_actions(self.widgets.content_backport, content_diff_actions)
        self.action_tracker.add_actions(self.widgets.content_upstream, content_diff_actions)
        self.action_tracker.add_actions(self.widgets.comment, comment_actions)
        for w, _ in self.widgets.content:
            self.action_tracker.add_actions(w, content_actions)

        self.action_tracker.set_action_enabled('find-file-next', False)
        self.action_tracker.set_action_enabled('find-file-prev', False)

        urwid.connect_signal(self.widgets.content_backport, 'moved',
                             functools.partial(self.save_position, 'a'))
        urwid.connect_signal(self.widgets.content_upstream, 'moved',
                             functools.partial(self.save_position, 'a'))
        urwid.connect_signal(self.widgets.comment, 'moved',
                             functools.partial(self.save_position, 'c'))
        urwid.connect_signal(self.widgets.comment, 'postchange', self.comment_changed)
        urwid.connect_signal(self.app.workplace, 'focus', self.save_comment)
        for i, (w, _) in enumerate(self.widgets.content):
            urwid.connect_signal(w, 'moved',
                                 functools.partial(self.save_position, i))

        self.action_tracker.set_action_enabled('fullscreen', self.layout.fullscreen_supported)
        main_widget = self.layout.setup()

        self.title_mode = None
        self.comment_on = None
        self.comment_needs_save = False
        self.full_activity = False
        self.last_search = None
        self.last_search_input = ''

        self.app.add_timer('save_comment', 10, self.save_comment)
        return main_widget

    def adjust_ui(self):
        """Adjusts the ui after the package was determined."""
        self.templates = self.pkg.get_edit_templates(self.app.lab)
        if self.templates:
            p = self.app.listbox_popup([h for _, h in self.templates])
            self.widgets.template_popup, self.widgets.templates = p
            self.action_tracker.add_actions(self.widgets.templates,
                                            [{ 'name': 'confirm-template' }])
        else:
            self.action_tracker.set_action_enabled('insert-template', False)

    def adjust_ui_mr_loaded(self):
        """Adjusts the ui after the MR got loaded."""
        if self.upstream_diff.file_list:
            p = self.app.listbox_popup(self.upstream_diff.file_list)
            self.widgets.file_popup, self.widgets.files = p
            self.action_tracker.add_actions(self.widgets.files,
                                            [{ 'name': 'confirm-find-file' }])

    def quit(self, **kwargs):
        self.app.del_timer('save_comment')
        self.save_comment()
        self.app.quit()

    def fetch(self):
        popup = self.app.wait_popup('Fetching commits...')
        self.repo.fetch()
        popup.stop()
        self.app.redraw_screen()

    def find_version_by_head(self, head_sha):
        for v in self.versions:
            if v.head_sha == head_sha:
                return v
        return None

    def mr_parse_user_list(self, data, action_name, user_key=None):
        result = []
        me = False
        user_id = self.app.lab.me['id']
        for user in data:
            if user_key:
                user = user_key(user)
            if user['id'] == user_id:
                me = True
            result.append(user)
        if self.mr['author']['id'] == user_id:
            # can't self-approve or self-assign
            self.action_tracker.set_action_enabled(action_name, False)
            self.action_tracker.set_action_enabled('un' + action_name, False)
        else:
            self.action_tracker.set_action_enabled(action_name, not me)
            self.action_tracker.set_action_enabled('un' + action_name, me)
        return result

    def mr_format_activity(self):
        user_id = self.app.lab.me['id']
        result = ui_markup.Markup()
        last_system = False
        diff_cache = {}
        major_sep = '─'
        minor_sep = '·'
        for date, act_type, content in self.activity:
            sep = major_sep
            date = '{:%Y-%m-%d %H:%M}'.format(date)
            if act_type == 'notes':
                for i, note in enumerate(content['notes']):
                    if not self.full_activity:
                        try:
                            if not self.pkg.filter_note(note, i == 0):
                                continue
                        except packages.PackageSkipThread:
                            break
                    if result and not (note['system'] and last_system):
                        result.append('\0{}\n'.format(sep))
                    last_system = note['system']

                    diff_quote = None
                    if note['type'] == 'DiffNote' and i == 0:
                        pos = note['position']
                        if pos['old_line'] is not None:
                            path = pos['old_path']
                            line = pos['old_line']
                            old = True
                        else:
                            path = pos['new_path']
                            line = pos['new_line']
                            old = False
                        diff_quote = [('desc', '\n'),
                                      ('diff-func', '{}:{}'.format(path, line)),
                                      ('desc', '\n')]
                        cache_key = pos['base_sha'] + pos['head_sha']
                        try:
                            cf = diff_cache[cache_key]
                        except KeyError:
                            parsed = self.repo.get_parsed_diff_range(pos['base_sha'],
                                                                     pos['head_sha'])
                            cf = formatters.CommitFormatter(self.mr['description'], parsed)
                            diff_cache[cache_key] = cf
                        diff = cf.format_diff_line(path, line, old)
                        if not diff:
                            diff = [('diff', '[code not available]\n')]
                        diff_quote += diff

                    date = '{:%Y-%m-%d %H:%M}'.format(gitlab.parse_datetime(note['created_at']))
                    body = formatters.replace_tabs(note['body'])[0]
                    if not body.endswith('\n'):
                        body += '\n'
                    if note['system']:
                        result.append(('desc-sys', '{}, '.format(date)))
                        # format some specific things specially
                        if body.startswith('marked this merge request as '):
                            body = body[29:].strip().strip('*')
                            result.append(('desc-sys', 'marked as '.format(date)))
                            result.append(('desc-version', body))
                            result.append(('desc-sys', '\n'))
                        else:
                            result.extend(self.format_name(note['author'], user_id,
                                                           system=True))
                            result.append(('desc-sys', ' {}'.format(body)))
                    else:
                        result.append(('desc-date', date))
                        result.append(('desc', ', '))
                        result.extend(self.format_name(note['author'], user_id))
                        if sep == major_sep and note['resolvable'] and not note['resolved']:
                            result.append(('desc', ' '))
                            result.append(('desc-err', 'blocking'))
                        if diff_quote:
                            result.extend(diff_quote)
                        result.append(('desc', '\n{}'.format(body)))
                    sep = minor_sep
            elif act_type == 'version':
                if result and not last_system:
                    result.append('\0{}\n'.format(sep))
                last_system = True
                result.append(('desc-sys', '{}, pushed '.format(date)))
                result.append(('desc-version', 'v{}'.format(content)))
                result.append(('desc-sys', '\n'))
            elif self.full_activity and act_type == 'label':
                if result and not last_system:
                    result.append('\0{}\n'.format(sep))
                last_system = True
                result.append(('desc-sys', '{}, '.format(date)))
                result.extend(self.format_name(content['user'], user_id, system=True))
                action = content['action']
                if action == 'add':
                    action = 'added'
                elif action == 'remove':
                    action = 'removed'
                label = content['label']['name'] if content['label'] else 'a deleted label'
                result.append(('desc-sys', ' {} {}\n'.format(action, label)))
        return result

    def mr_format_add_user_list(self, annot, user_list, name):
        if not user_list:
            return
        my_id = self.app.lab.me['id']
        annot.add(('desc-header', name + ':'))
        for i, user in enumerate(user_list):
            data = self.format_name(user, my_id)
            if i < len(user_list) - 1:
                data.append(',')
            annot.add(data)
        annot.add_break()

    def mr_format_cover_annotation(self):
        my_id = self.app.lab.me['id']
        annot = formatters.AnnotationFormatter('desc')
        annot.add(('desc-version', 'v{}'.format(self.latest_version.number)))
        annot.add('from')
        annot.add(('desc-date', '{:%Y-%m-%d %H:%M}'.format(self.latest_version.created_at)))
        annot.add_break()
        annot.add(('desc-header', 'Author:'))
        annot.add(self.format_name(self.mr['author'], my_id,
                                   email=self.mr.author['public_email']))
        annot.add_break()

        data = self.mr_parse_user_list(self.mr['assignees'], 'assign')
        self.mr_format_add_user_list(annot, data, 'Assigned to')
        data = self.mr_parse_user_list(self.mr.approvals['approved_by'],
                                       action_name='approve',
                                       user_key=lambda u: u['user'])
        self.mr_format_add_user_list(annot, data, 'Approved by')

        if (self.info['approved_version'] and
            self.info['approved_version'] < self.latest_version.number):
            annot.add(('desc', 'You last approved'))
            annot.add(('desc-version', 'v{}'.format(self.info['approved_version'])))
            annot.add_break()
        annot.add_empty_line()
        if self.mr['state'] != 'opened':
            annot.add(('desc-label-err', self.mr['state']), label=True)
        if self.mr['work_in_progress']:
            annot.add(('desc-label-err', 'draft'), label=True)
        if self.unresolved_threads:
            plural = 's' if self.unresolved_threads > 1 else ''
            annot.add(('desc-label-err',
                       ' {} blocking thread{} '.format(self.unresolved_threads, plural)))
        annot.add_break()
        for label in self.mr['labels']:
            cls = self.pkg.get_label_color(label)
            if cls is None:
                continue
            if not cls:
                cls = 'desc-label'
            else:
                cls = 'desc-label-' + cls
            annot.add((cls, label), label=True)
        return annot.format()

    def remove_diffstat(self, data):
        return re.sub(r'(\n [^ ].*)+\n \d* files? changed(, \d* insertions?\(\+\))?(, \d* deletions?\(-\))?',
                      '', data)

    def mr_parse_info(self):
        self.info = {}
        self.info['bugzilla'] = self.pkg.get_bugzilla(self.mr)
        if not self.info['bugzilla']:
            self.action_tracker.set_action_enabled('bugzilla', False)

        user_id = self.app.lab.me['id']
        last_version = 1
        last_approval = None
        for _, act_type, content in self.activity:
            if act_type == 'version':
                last_version = content
            elif act_type == 'notes':
                for note in content['notes']:
                    if note['system'] and note['author']['id'] == user_id:
                        if note['body'] == 'approved this merge request':
                            last_approval = last_version
                        elif note['body'] == 'unapproved this merge request':
                            last_approval = None
        self.info['approved_version'] = last_approval
        if not last_approval or last_approval >= self.latest_version.number:
            self.action_tracker.set_action_enabled('compare-last-approved-version', False)

    def mr_get_disp_commit_list(self, old_version=None):
        if old_version:
            cls = mr_commits.DispCommitVersionList
            version = (old_version, self.latest_version)
        else:
            cls = mr_commits.DispCommitList
            version = self.latest_version
        return cls(version, self.repo, self.repos, self.app, self.mr,
                   self.mr_format_cover_annotation(), self.mr_format_activity())

    def format_mark(self, mark, cls_base='list'):
        cls = cls_base
        if mark.isupper() or mark in ('+', '-', '!'):
            cls = cls_base + '-problem'
        elif mark == '✎':
            cls = cls_base + '-info'
        return (cls, mark)

    def format_name(self, user, my_id, email=None, system=False):
        if user['id'] == my_id:
            cls1 = cls2 = 'me'
        elif system:
            cls1 = cls2 = 'sys'
        else:
            cls1 = 'person'
            cls2 = 'username'
        result = [('desc-' + cls1, user['name']),
                  ('desc-' + cls2, ' @' + user['username'])]
        if email:
            result.insert(1, ('desc-' + cls1, ' <{}>'.format(email)))
        return result

    def get_long_help(self):
        result = [
            ('title', 'Commit flags:\n\n'),
            self.format_mark('C'), '  a code conflict (not a clean cherry pick)\n',
            self.format_mark('·'), '  a context conflict\n',
            self.format_mark('R'), '  RHEL only patch\n',
            self.format_mark('!'), '  no upstream reference\n',
            self.format_mark('?'), '  unknown upstream commit\n',
        ]
        for mark, desc, _ in self.pkg.get_marks_help():
            result.append(self.format_mark(mark))
            result.append('  {}\n'.format(desc))
        result.extend([
            self.format_mark('✎'), '  has an unsubmitted comment\n',
        ])
        return result

    def format_mark_list(self, marks, cls_base='list', include_empty=True):
        result = []
        for i in range(self.commits.num_marks):
            try:
                m = marks[i]
            except IndexError:
                m = ' '
            if m == ' ' and not include_empty:
                continue
            result.append(self.format_mark(m, cls_base))
        return result

    def format_commit_item(self, commit):
        digits = len(str(self.commits.count))
        if commit.is_type_with_index() or commit.type == 'removed':
            index = '' if commit.type == 'removed' else commit.index
            label = [
                ('list', '{:{}} '.format(index, digits)),
                ('list-sha', commit.get_sha(6)),
                ('list', ' '),
            ]
            label.extend(self.format_mark_list(commit.marks))
            label.append(('list', ' {}'.format(commit.name)))
        elif commit.type == 'suggested':
            label = [
                ('list', ' ' * (digits + 9 + self.commits.num_marks) + '└─ '),
                ('list-sub', commit.category),
                ('list', ' '),
                ('list-sha', commit.get_sha(12)),
                ('list', ' {}'.format(commit.name)),
            ]
        else:
            assert False
        return label

    def update_commit_item(self, commit):
        digits = len(str(self.commits.count))
        commit.widget.set_text(self.format_commit_item(commit))
        self.update_title(commit_changed=True)

    def get_notes(self):
        threads = self.mr.discussions()
        self.unresolved_threads = 0
        for thread in threads:
            for note in thread['notes']:
                if note['resolvable'] and not note['resolved']:
                    self.unresolved_threads += 1
                    break
        return threads

    def mr_load_activity(self):
        discussions = [(gitlab.parse_datetime(d['notes'][0]['created_at']),
                        'notes', d)
                       for d in self.get_notes()]
        labels = [(gitlab.parse_datetime(e['created_at']), 'label', e)
                  for e in self.mr.resource_label_events()]
        lab_versions = self.mr.versions()
        self.versions = [git.MRVersion(self.repo, v, base_sha, i+1, len(lab_versions))
                         for i, (v, base_sha) in enumerate(lab_versions)]
        self.version_diffs = [None] * (len(self.versions) - 1)
        a_versions = [(v.created_at, 'version', v.number)
                      for v in self.versions
                      if v.number > 1]
        self.activity = discussions + labels + a_versions
        self.activity.sort(key=lambda x: x[0])

        # populate versions
        for v in self.versions:
            v.set_shas(self.project, self.mr, self.activity)
        self.latest_version = self.versions[-1]
        self.action_tracker.set_action_enabled('compare-versions', len(self.versions) > 1)

    def load_mr(self, fetch=False):
        if fetch:
            self.fetch()
        popup = self.app.wait_popup('Loading the merge request...')
        try:
            self.mr = self.project.mr(self.mr_id)
            self.mr.fetch()
        except gitlab.NotFoundError:
            popup.stop()
            self.app.message_popup(self.quit, 'Merge request !{} was not found.'.format(self.mr_id))
            return
        try:
            if self.mr['diff_refs']['head_sha'] != self.repo.get_mr_head(self.mr_id):
                self.mr = None
        except KeyError:
            self.mr = None
        if not self.mr:
            popup.stop()
            self.app.confirm('git-fetch', 'The local git data is outdated. Refresh?',
                             self.load_mr, True, reject_callback=self.quit)
            return
        # prefetch the author
        self.mr.author
        self.mr.data['description'] = self.remove_diffstat(self.mr['description'])

        self.mr_load_activity()
        popup.stop()

        self.mr_parse_info()
        self.upstream_diff = self.mr_get_disp_commit_list()
        self.adjust_ui_mr_loaded()
        self.switch_commits()

    def switch_commits(self, ver=None):
        self.save_comment()
        self.current_version = ver
        if ver is None:
            self.commits = self.upstream_diff
        else:
            ver -= 1
            if self.version_diffs[ver] is None:
                self.version_diffs[ver] = self.mr_get_disp_commit_list(self.versions[ver])
            self.commits = self.version_diffs[ver]
        self.layout.notify_commit_list_items(len(self.commits))
        walker = ui.PrettyListWalker((self.format_commit_item(c) for c in self.commits),
                                     self.app.focus_map, self.app.passive_map)
        for c, w in zip(self.commits, walker):
            c.set_widget(w.original_widget)
        urwid.connect_signal(walker, 'modified', self.show_commit)
        self.widgets.commit_list.body = walker

        self.load_comments()

        self.update_title(mr_changed=True, commit_changed=True)
        self.show_commit()
        if self.upstream_diff.file_list:
            self.action_tracker.set_action_enabled('find-file-next', ver is None)
            self.action_tracker.set_action_enabled('find-file-prev', ver is None)

    def load_comments(self):
        self.comment_count = 0
        self.action_tracker.set_action_enabled('submit-comments', False)

        self.comment_storage = CommentStorage(self.app.get_config_dir(),
                                              self.repo, self.mr['iid'])
        oid_map = { c.get_sha(): c for c in self.commits
                                   if c.type == 'backport' }
        upstream_map = { c.upstream: c for c in self.commits
                                       if c.type == 'backport' and c.upstream }
        name_map = { c.name: c for c in self.commits
                               if c.type == 'backport' }
        for oid in self.comment_storage:
            changed = False
            combined = False
            name, upstream, text = self.comment_storage[oid]
            if oid == self.comment_storage.GENERAL:
                commit = self.commits[0]
            elif oid in oid_map:
                commit = oid_map[oid]
            elif upstream in upstream_map:
                # the previous commit id is not present anymore, try to
                # match on upstream commit id
                commit = upstream_map[upstream]
                changed = True
            elif name in name_map:
                # nope, try to match on commit name
                commit = name_map[name]
                changed = True
            else:
                # no luck, append as a general MR comment
                fmt = formatters.CommentFormatter(self.pkg)
                text = fmt.format(oid, name, text, upstream=upstream,
                                  prefix='from a previous version')
                commit = self.commits[0]
                if commit.comment:
                    text = fmt.join((commit.comment, text))
                combined = True
                changed = True
            if not combined and commit.comment:
                commit.comment = '{}\n\n{}'.format(commit.comment, text)
            else:
                commit.comment = text
            self.update_comment_mark(commit, True)

            if changed:
                self._save_comment(commit)
                del self.comment_storage[oid]

    def save_position(self, where, position, x, y):
        try:
            commit = self.commits[self.widgets.commit_list.focus_position]
        except (AttributeError, IndexError):
            # fired during init, ignore
            return
        commit.set_pos(where, position)

    def update_comment_mark(self, commit, active):
        new_mark = '✎' if active else ' '
        if commit.get_mark(self.commits.num_marks - 1) == new_mark:
            return

        commit.set_mark(self.commits.num_marks - 1, new_mark)
        self.update_commit_item(commit)

        if active:
            if self.comment_count == 0:
                self.action_tracker.set_action_enabled('submit-comments', True)
            self.comment_count += 1
        else:
            self.comment_count -= 1
            if self.comment_count == 0:
                self.action_tracker.set_action_enabled('submit-comments', False)

    def comment_changed(self, widget, old_text):
        self.comment_needs_save = True

    def _save_comment(self, commit):
        if commit.type == 'backport':
            upstream = commit.upstream or ''
            key = commit.get_sha()
        else:
            assert commit.type == 'cover'
            upstream = ''
            key = self.comment_storage.GENERAL
        self.comment_storage[key] = (commit.name, upstream, commit.comment)

    def save_comment(self):
        if not self.comment_on or not self.comment_needs_save:
            return True
        text = self.widgets.comment.get_edit_text()
        self.comment_on.comment = text
        self.update_comment_mark(self.comment_on, text != '')

        self._save_comment(self.comment_on)
        self.comment_needs_save = False
        return True

    def _submit_comments(self, button=None):
        if button is None or button == 'no':
            return
        data = []
        fmt = formatters.CommentFormatter(self.pkg)
        for commit in self.commits:
            if not commit.is_type_with_index():
                continue
            if not commit.comment:
                continue
            if commit.type == 'backport':
                data.append(fmt.format(commit.get_sha(),
                                       commit.name,
                                       commit.comment,
                                       number=(commit.index, self.commits.count),
                                       upstream=commit.upstream))
            else:
                data.append(fmt.format_main(commit.comment))
        data = fmt.join(data)

        if button == 'preview':
            self.app.view_popup(self.submit_comments, data)
            return

        popup = self.app.wait_popup('Submitting comments...')
        self.mr.notes['body'] = data
        self.mr.notes.save()
        for commit in self.commits:
            if not commit.is_type_with_index():
                continue
            if not commit.comment:
                continue
            commit.comment = ''
            self._save_comment(commit)
            self.update_comment_mark(commit, False)
        popup.stop()

    def submit_comments(self):
        self.save_comment()
        self.app.buttons_popup(self._submit_comments,
                               'Submit the comments to GitLab?',
                                ('~yes', '~preview', '~no'),
                                allow_esc=True)

    def _compare_versions(self, text=None):
        if text is None:
            return
        if text == '':
            self.switch_commits()
            return
        try:
            ver = int(text)
        except ValueError:
            ver = 0
        if ver < 1 or ver >= len(self.versions):
            self.app.message_popup(self.compare_versions, 'Invalid version',
                                   callback_args=(text,))
            return
        self.switch_commits(ver)

    def compare_versions(self, text='', **kwargs):
        end = len(self.versions) - 1
        self.app.edit_popup(self._compare_versions,
                            'Version to compare (1-{}, empty to upstream):'.format(end),
                            edit_text=text, allow_esc=True)

    def update_cover(self):
        self.set_commit_show_data(0, 'annot', self.mr_format_cover_annotation())

    def approve(self, approve=True):
        popup = self.app.wait_popup('Working...')
        if approve:
            try:
                self.mr.approve(self.latest_version.head_sha)
            except gitlab.ConflictError:
                popup.stop()
                self.app.message_popup(None, 'The MR got force-pushed meanwhile. Not approving.')
                return
        else:
            self.mr.unapprove()
        self.update_cover()
        popup.stop()

    def assign(self, assign=True):
        popup = self.app.wait_popup('Working...')
        self.mr.assign(assign)
        self.update_cover()
        popup.stop()

    def add_todo(self):
        popup = self.app.wait_popup('Working...')
        try:
            self.mr.todo()
        except gitlab.NotModifiedError:
            popup.stop()
            self.app.message_popup(None, 'Already on the TODO list.')
            return
        popup.stop()

    def switch_full_activity(self):
        self.full_activity = not self.full_activity
        self.set_commit_show_data(0, 'disc', self.mr_format_activity())

    def update_title(self, mr_changed=False, commit_changed=False):
        new_title_mode = self.layout.get_title_mode()
        if (new_title_mode == self.title_mode and
                not (self.title_mode == 'mr' and mr_changed) and
                not (self.title_mode == 'commit' and commit_changed)):
            return
        self.title_mode = new_title_mode

        marks = None
        if self.title_mode == 'commit':
            c = self.commits[self.widgets.commit_list.focus_position]
            if c.is_type_with_index():
                commits = '{}/{}'.format(c.index, self.commits.count)
                marks = self.format_mark_list(c.marks, 'header', include_empty=False)
            elif c.type == 'removed':
                commits = 'removed'
                marks = self.format_mark_list(c.marks, 'header', include_empty=False)
            else:
                assert c.type == 'suggested'
                commits = c.category
            name = c.name
        else:
            commits = '{} commit{}'.format(self.commits.count,
                                           's' if self.commits.count > 1 else '')
            name = utils.oneline(self.mr['title'])
        if self.current_version:
            version = 'v{}→v{}'.format(self.current_version, self.latest_version.number)
        elif self.latest_version.number > 1:
            version = 'v{}'.format(self.latest_version.number)
        else:
            version = None

        title = [
            ('header', ' !{} '.format(self.mr['iid'])),
            ('header-branch', ' {}/{} '.format(self.repo.short_project, self.mr['target_branch'])),
        ]
        if version:
            title.append(('header-version', ' {} '.format(version)))
        title.append(('header-commits', ' {} '.format(commits)))
        if marks:
            title.append(('header', ' '))
            title.extend(marks)
        title.append(('header', ' {} '.format(name)))
        self.app.set_header(title)

    def set_subtitle(self, commit):
        if not commit:
            self.widgets.comment_header.set_text('')
            return
        if commit.type == 'cover':
            subtitle = [
                ('subheader', ' comment on the merge request'),
            ]
        else:
            assert commit.type == 'backport'
            subtitle = [
                ('subheader', ' comment on '),
                ('subheader-commit', commit.get_sha(12)),
                ('subheader', ' {}'.format(commit.name)),
            ]
        self.widgets.comment_header.set_text(subtitle)

    def set_all_content(self, commit):
        sep = {
            'annot': '\n',
            'header': '\n',
            'stat': '---\n',
            'diff': '\n',
            'disc': '\0─\n',
        }
        for i, (widget, vals) in enumerate(self.widgets.content):
            if 'disc' in vals:
                widget.wrap = commit.wrap
            text = ui_markup.Markup()
            for v in vals:
                key = key_all = v
                if key.endswith('_all'):
                    key = key[:-4]
                data = commit.show.get(key) or commit.show.get(key_all)
                if not data:
                    continue
                if text:
                    text.append(('desc', sep[key]))
                text.extend(data)
            widget.set_markup_text(text or '')
            widget.edit_pos = commit.get_pos(i)

    def set_commit_show_data(self, index, show_type, data):
        commit = self.commits[index]
        commit.show[show_type] = data
        if index != self.widgets.commit_list.focus_position:
            return
        self.set_all_content(commit)

    def show_commit(self):
        index = self.widgets.commit_list.focus_position
        commit = self.commits[index]
        self.set_all_content(commit)
        if commit.has_comparison():
            self.widgets.content_backport.set_markup_text(commit.comparison[0])
            self.widgets.content_upstream.set_markup_text(commit.comparison[1])
            self.widgets.content_backport.edit_pos = commit.get_pos('a')
            self.widgets.content_upstream.edit_pos = commit.get_pos('a')
            self.action_tracker.set_action_enabled('interdiff', True)
            self.action_tracker.set_action_enabled('interdiff-alt', True, optional=True)
            self.layout.enable_content_compare(True)
        else:
            self.widgets.content_backport.set_markup_text('')
            self.widgets.content_upstream.set_markup_text('')
            self.action_tracker.set_action_enabled('interdiff', False)
            self.action_tracker.set_action_enabled('interdiff-alt', False, optional=True)
            self.layout.enable_content_compare(False)
        self.save_comment()
        if commit.is_type_with_index():
            self.comment_on = commit
            self.set_subtitle(commit)
            self.widgets.comment.set_initial_edit_text(commit.comment)
            self.comment_needs_save = False
            self.widgets.comment.edit_pos = commit.get_pos('c')
            self.action_tracker.set_action_enabled('comment', True)
            self.layout.enable_comment(True)
        else:
            self.comment_on = None
            self.set_subtitle(None)
            self.widgets.comment.set_initial_edit_text('')
            self.comment_needs_save = False
            self.action_tracker.set_action_enabled('comment', False)
            self.layout.enable_comment(False)
        self.action_tracker.set_action_enabled('full-activity', index == 0)
        self.app.update_help()
        self.update_title(commit_changed=True)

    def scroll_commits(self, amount):
        try:
            self.widgets.commit_list.focus_position += amount
        except IndexError:
            pass

    def switch_to_content_compare(self, alt):
        if self.layout.alternative_compare:
            self.layout.switch_to_content_compare(alt)
        else:
            self.layout.switch_to_content_compare()
        self.update_title()

    def search_start(self, **kwargs):
        # There should always be focus. But if it's not, it would make a poor
        # user experience to present the dialog box. Better check.
        focus = self.app.workplace.get_focus()
        if not focus:
            return
        self.app.edit_popup(self._search_new, 'Search for (regex)', self.last_search_input,
                            allow_esc=True)

    def _search_new(self, text=None):
        if not text:
            return
        self.last_search_input = text
        try:
            self.last_search = re.compile(text)
        except re.error as e:
            self.app.message_popup(self.search_start,
                                   'Invalid regular expression: {}'.format(e.msg))
            return
        self._search_next()

    def _search_next(self):
        focus = self.app.workplace.get_focus()
        if not focus:
            return
        match = self.last_search.search(focus.edit_text, focus.edit_pos + 1)
        if not match:
            self.app.message_popup(None, 'Not found.')
        else:
            focus.edit_pos = match.start()

    def search_next(self):
        if self.last_search:
            self._search_next()
        else:
            self.search_start()

    def search_next_diff(self, prev=False):
        commit = self.commits[self.widgets.commit_list.focus_position]
        focus = self.app.workplace.get_focus()
        if focus == self.widgets.content_backport:
            idx = 0
        elif focus == self.widgets.content_upstream:
            idx = 1
        else:
            return
        pos = focus.edit_pos
        jump_list = commit.comparison[2]
        if prev:
            jump_list = reversed(jump_list)
        for jump in jump_list:
            new_pos = jump[idx]
            if (prev and new_pos < pos) or (not prev and new_pos > pos):
                focus.edit_pos = new_pos
                return

    def show_templates(self):
        self.app.popup_show(self.widgets.template_popup)

    def insert_template(self):
        index = self.widgets.templates.focus_position
        self.widgets.template_popup.stop()
        self.widgets.comment.insert_text(self.templates[index][0])

    def show_find_file(self, backwards=False):
        self.app.popup_show(self.widgets.file_popup)
        self.find_file_backwards = backwards

    def find_file(self):
        path = self.upstream_diff.file_list[self.widgets.files.focus_position]
        self.widgets.file_popup.stop()
        if self.find_file_backwards:
            finish = -1
            step = -1
        else:
            finish = len(self.commits)
            step = 1
        start = self.widgets.commit_list.focus_position + step
        for i in range(start, finish, step):
            if self.commits[i].touches_file(path):
                break
        else:
            self.app.message_popup(None, 'No more matches.')
            return
        self.widgets.commit_list.focus_position = i

    def action(self, what):
        if what == 'quit':
            self.quit()
        elif what == 'next-item':
            self.scroll_commits(1)
        elif what == 'prev-item':
            self.scroll_commits(-1)
        elif what == 'to-content':
            self.layout.switch_to_commit()
            self.update_title()
        elif what == 'from-content':
            self.layout.switch_from_content()
        elif what in ('to-commits', 'comment-to-commits'):
            self.layout.switch_to_commit_list()
            self.update_title()
        elif what == 'bugzilla':
            for b in self.info['bugzilla']:
                self.app.web_browser(b)
        elif what == 'gitlab':
            self.app.web_browser(self.mr['web_url'])
        elif what == 'fullscreen':
            self.layout.switch_to_fullscreen()
            self.update_title()
        elif what in ('interdiff', 'interdiff-alt'):
            self.switch_to_content_compare(what == 'interdiff-alt')
        elif what == 'comment':
            self.layout.switch_to_comment()
            self.update_title()
        elif what == 'compare-versions':
            self.compare_versions()
        elif what == 'compare-last-approved-version':
            self._compare_versions(self.info['approved_version'])
        elif what == 'submit-comments':
            self.submit_comments()
        elif what == 'approve':
            self.approve(True)
        elif what == 'unapprove':
            self.approve(False)
        elif what == 'assign':
            self.assign(True)
        elif what == 'unassign':
            self.assign(False)
        elif what == 'todo-add':
            self.add_todo()
        elif what == 'ci-status':
            mr_ci.MRCIStatus(self.app, self.mr).fetch()
        elif what == 'find-file-next':
            self.show_find_file()
        elif what == 'find-file-prev':
            self.show_find_file(backwards=True)
        elif what == 'confirm-find-file':
            self.find_file()
        elif what == 'full-activity':
            self.switch_full_activity()
        elif what == 'search':
            self.search_start()
        elif what == 'search-next':
            self.search_next()
        elif what == 'diff-next':
            self.search_next_diff()
        elif what == 'diff-prev':
            self.search_next_diff(prev=True)
        elif what == 'insert-template':
            self.show_templates()
        elif what == 'confirm-template':
            self.insert_template()
