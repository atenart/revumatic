import urwid

from modules import ui

class LayoutBase:
    """Base class for layout definition.
    Class attributes:
    fullscreen_supported - True if the layout supports the fullscreen action.
    alternative_compare - a string if the layout supports an alternative way
                          of displaying the content compare (e.g.
                          horizontal); the content of the string is a help
                          text.
    content_enter_supported - True if the layout does something with the
                              Enter key pressed in a content widget.
    """
    fullscreen_supported = False
    alternative_compare = False
    content_enter_supported = False

    def __init__(self, workplace, commit_list, backport, upstream, comment):
        self.workplace = workplace
        self.commit_list = commit_list
        self.backport = backport
        self.upstream = upstream
        self.comment = comment

    def create_content_widgets(self):
        """You may redefine this in your subclass. It should allocate and
        return all content widgets used by the layout. The widgets must be
        of type ui.BoxedEdit(readonly=True). The return value is a tuple
        of tuples (widget, show), where show is a tuple of values to show in
        this content widget. The available values are:
        "annot" - include annotation (meta data) about the commit/cover
                  letter.
        "header" - include commit description/cover letter text.
        "stat" - include diffstat.
        "disc" - include discussion; non-empty only for the cover letter.
        "diff" - include the diff; empty for the cover letter.
        "diff_all" - include the diff, both for commits and the cover
        letter.
        Note special behavior: if "disc" is present, the widget will be
        automatically switched to line wrap mode whenever a discussion is
        present. This makes it a bad idea to mix disc and diff_all."""
        self.content = ui.BoxedEdit(readonly=True)
        return ((self.content, ('annot', 'header', 'stat', 'diff', 'disc')),)

    def setup(self):
        """Redefine in your subclass. It should arrange all the widgets and
        return a single widget that will be used as the workplace."""
        raise NotImplementedError

    def notify_commit_list_items(self, number):
        """You may redefine this in your subclass if you're interested in
        the number of items in the commit list, once it is known."""
        return

    def enable_content_compare(self, enable):
        """Redefine in your subclass. It should enable or disable the
        backport and upstream widgets. When called with False, this method
        must hide those widgets. When called with True, the method may show
        those widgets or keep them hidden, depending on the layout needs."""
        raise NotImplementedError

    def enable_comment(self, enable):
        """Redefine in your subclass. It should enable or disable the
        comment widget. When called with False, this method must hide the
        widget. When called with True, the method may show the widget or
        keep it hidden, depending on the layout needs."""
        raise NotImplementedError

    def switch_to_commit(self):
        """Redefine in your subclass. It is called as a reaction to user
        selecting a commit from the commit list. The method should switch
        focus to the commit details. It is up to the layout to decide what
        "commit details" means; it could be any of the content, backport,
        upstream or comment widgets."""
        raise NotImplementedError

    def switch_to_commit_list(self):
        """Redefine in your subclass. It is called as a reaction to user
        pressing Esc in the content, backport, upstream or commit widgets.
        The method should switch to the commit list, showing the list if it
        was previously hidden."""
        raise NotImplementedError

    def switch_to_content_compare(self):
        """Redefine in your subclass. It is called as a reaction to the 'u'
        key. It should switch focus to the content comparison (backport vs.
        upstream widgets), showing the widgets if they were previously
        hidden, or switch the focus from the content comparison. You can
        assume that this will never be called if a previous call to
        enable_content_compare disabled the widgets. If you set
        alternative_compare, this method will be called with an additional
        bool parameter set to True if the alternative content compare should
        be used."""
        raise NotImplementedError

    def switch_to_comment(self):
        """Redefine in your subclass. It is called as a reaction to the 'c'
        key. It should switch focus to the comment widget, showing the
        widget if it was previously hidden. You can assume that this will
        never be called if a previous call to enable_comment disabled the
        widget."""
        raise NotImplementedError

    def switch_to_fullscreen(self):
        """Redefine in your subclass if you set fullscreen_supported to
        True. Otherwise, you can leave this not implemented. It is called as
        a reaction to the 'f' key. It should switch some widget (usually the
        focused one but it's not required) to or from fullscreen mode."""
        raise NotImplementedError

    def switch_from_content(self):
        """If you set content_enter_supported, you must redefine this in
        your subclass. It is called whenever the Enter key is pressed in any
        of the content widgets."""
        raise NotImplementedError

    def get_title_mode(self):
        """You may redefine this in your subclass. It gets called to obtain
        the mode of the title bar. Available return values are 'mr' and
        'commit', for details about the merge request or details about the
        selected commit, respectively. The 'mr' value is useful when the
        commit list is visible, the 'commit' value when it is hidden."""
        return 'mr'


class StandardLayout(LayoutBase):
    fullscreen_supported = True
    alternative_compare = 'horizontal split'
    commit_list_max = 25

    def create_panels(self):
        return ui.PileMax((
            self.top,
            ('pack', urwid.AttrMap(urwid.Divider('─'), 'divider')),
            self.bottom))

    def setup(self):
        self.top = ui.JailedPlaceholder(self.commit_list)
        self.bottom = ui.JailedPlaceholder(self.content)
        self.top_mode = 0
        self.bottom_mode = 0
        self.panels = self.create_panels()
        backport_wrapper = ui.JailedPlaceholder(self.backport)
        upstream_wrapper = ui.JailedPlaceholder(self.upstream)
        self.multi_diff_hor = urwid.Columns((
            backport_wrapper,
            (1, urwid.SolidFill('│')),
            upstream_wrapper))
        self.multi_diff_vert = urwid.Pile((
            backport_wrapper,
            (1, urwid.SolidFill('─')),
            upstream_wrapper))

        self.main = ui.Placeholder(self.panels)
        self.fullscreen = False
        self.commit_list_items = None

        return self.main

    def notify_commit_list_items(self, number):
        new_size = self.commit_list_max
        if number is not None:
            self.commit_list_items = number
            new_size = min(number, new_size)
        self.panels.set_max_size(0, new_size)

    def _set_bottom_mode(self, mode):
        if mode == 0:
            self.bottom.original_widget = self.content
        elif mode == 1:
            self.bottom.original_widget = self.multi_diff_hor
        else:
            self.bottom.original_widget = self.multi_diff_vert

    def _set_top_mode(self, mode):
        if mode == 0:
            self.top.original_widget = self.commit_list
            self.notify_commit_list_items(self.commit_list_items)
        else:
            self.top.original_widget = self.comment
            self.notify_commit_list_items(None)

    def _switch_comment(self, enable):
        if enable:
            self._switch_fullscreen(force_off=True)
            self._set_top_mode(1)
            self.workplace.focus_to(self.comment)
            self.top_mode = 1
        elif self.top_mode != 0:
            self._set_top_mode(0)
            self.top_mode = 0

    def _switch_fullscreen(self, force_off=False):
        if not self.fullscreen and force_off:
            return
        if self.fullscreen:
            self.main.original_widget = self.panels
        else:
            self.main.original_widget = self.bottom
        self.fullscreen = not self.fullscreen

    def enable_content_compare(self, enable):
        self._set_bottom_mode(self.bottom_mode if enable else 0)

    def enable_comment(self, enable):
        self._set_top_mode(self.top_mode if enable else 0)

    def switch_to_commit(self):
        self.workplace.focus_to(self.bottom)

    def switch_to_commit_list(self):
        self._switch_fullscreen(force_off=True)
        self._switch_comment(False)
        self.workplace.focus_to(self.commit_list)

    def switch_to_content_compare(self, alternative):
        new_bottom_mode = 2 if alternative else 1
        self.bottom_mode = 0 if self.bottom_mode == new_bottom_mode else new_bottom_mode
        self._set_bottom_mode(self.bottom_mode)

    def switch_to_comment(self):
        self._switch_comment(True)

    def switch_to_fullscreen(self):
        self._switch_fullscreen()

    def get_title_mode(self):
        if self.fullscreen or self.top.original_widget == self.comment:
            return 'commit'
        return 'mr'


class WideLayout(StandardLayout):
    def create_panels(self):
        panels = ui.ColumnsMax((
            self.top,
            (1, urwid.AttrMap(urwid.SolidFill('│'), 'divider')),
            self.bottom))
        panels.set_max_size(0, 80)
        return panels

    def notify_commit_list_items(self, number):
        return

class LargeLayout(LayoutBase):
    content_enter_supported = True

    def setup(self):
        self.comment_wrapper = ui.JailedPlaceholder(self.comment)
        self.left = ui.PileMax((
            ui.JailedPlaceholder(self.commit_list),
            ('pack', urwid.AttrMap(urwid.Divider('─'), 'divider')),
            ('weight', 2, ui.JailedPlaceholder(self.content)),
            ('pack', urwid.AttrMap(urwid.Divider('─'), 'divider')),
            self.comment_wrapper))
        self.content_compare = urwid.Columns((
            ui.JailedPlaceholder(self.backport),
            (1, urwid.SolidFill('│')),
            ui.JailedPlaceholder(self.upstream)))
        self.right = ui.JailedPlaceholder(self.content_compare)
        self.main = ui.ColumnsMax((
            self.left,
            (1, urwid.AttrMap(urwid.SolidFill('│'), 'divider')),
            self.right))
        self.main.set_max_size(0, 80)

        self.disabled_comment = urwid.SolidFill(' ')

        return self.main

    def create_content_widgets(self):
        self.content = ui.BoxedEdit(readonly=True)
        self.diff = ui.BoxedEdit(readonly=True)
        return ((self.content, ('annot', 'header', 'stat', 'disc')),
                (self.diff, ('diff_all',)))

    def notify_commit_list_items(self, number):
        self.left.set_max_size(0, min(number, 25))

    def enable_content_compare(self, enable):
        if enable:
            self.right.original_widget = self.content_compare
        else:
            self.right.original_widget = self.diff

    def enable_comment(self, enable):
        if enable:
            self.comment_wrapper.original_widget = self.comment
        else:
            self.comment_wrapper.original_widget = self.disabled_comment

    def switch_to_commit(self):
        self.workplace.focus_to(self.content)

    def switch_to_commit_list(self):
        self.workplace.focus_to(self.commit_list)

    def switch_to_content_compare(self):
        if self.workplace.get_focus() == self.backport:
            self.workplace.focus_to(self.upstream)
        else:
            self.workplace.focus_to(self.backport)

    def switch_to_comment(self):
        self.workplace.focus_to(self.comment)

    def switch_from_content(self):
        if self.workplace.get_focus() == self.content:
            self.workplace.focus_to(self.right)
