import urwid

from modules import keys


class DictMap(dict):
    """A dictionary that returns the key for non-existent items."""
    def __getitem__(self, key):
        return self.get(key, key)


class Settings:
    def apply_config(self, config):
        self.key_map = keys.Keys(config['keys'] if 'keys' in config else {})
        self.set_command_map()
        self.format_tab = config.get('format', 'tab', fallback='') + '  '
        self.format_trail = config.get('format', 'trail', fallback='') + ' '

    def set_command_map(self):
        cmd_map = DictMap()
        for name in dir(self.key_map):
            if name.startswith('cursor_') or name in ('redraw_screen',
                                                      'activate'):
                store_name = name.replace('_', ' ')
                for key in self.key_map[name]:
                    cmd_map[key] = store_name
        urwid.command_map.command_map = cmd_map
        urwid.main_loop.command_map = cmd_map
        urwid.Widget._command_map = cmd_map


settings = Settings()
