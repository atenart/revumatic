import os
import signal
import urwid
from urwid.command_map import (CURSOR_LEFT, CURSOR_RIGHT, CURSOR_UP,
                               CURSOR_DOWN, CURSOR_MAX_LEFT, CURSOR_MAX_RIGHT,
                               CURSOR_PAGE_UP, CURSOR_PAGE_DOWN)
import weakref

from modules.keys import display_keys
from modules.settings import settings


class clipboard:
    text = None


class EnhancedMainLoop(urwid.MainLoop):
    def __init__(self, *args, on_start=(), **kwargs):
        super().__init__(*args, **kwargs)
        self._on_start = on_start
        self.catch_signals()

    def catch_signals(self):
        def defer_to_sync(signalnum, stack):
            os.write(pipe, b'\0')

        def sigtstp(data):
            self.process_input(('ctrl z',))

        pipe = self.watch_pipe(sigtstp)
        self.event_loop.set_signal_handler(signal.SIGTSTP, defer_to_sync)

    def start(self):
        result = super().start()
        for func in self._on_start:
            func()
        return result


class ActionTracker:
    def __init__(self, confirm_func, action_callback):
        self.widgets = weakref.WeakSet()
        self.callback = weakref.WeakMethod(action_callback)
        self.confirm_func = confirm_func

    def add_actions(self, widget, actions):
        widget.add_actions(actions)
        if widget in self.widgets:
            return
        self.widgets.add(widget)
        urwid.connect_signal(widget, 'action', self._callback)

    def set_action_enabled(self, action_name, enabled, optional=False):
        count = 0
        for widget in self.widgets:
            try:
                widget.set_action_enabled(action_name, enabled)
                count += 1
            except KeyError:
                pass
        if not count and not optional:
            raise KeyError('{} not found in actions'.format(action_name))

    def _callback(self, name, extra={}):
        callback = self.callback()
        if not callback:
            return
        confirm = extra.get('confirm')
        if confirm:
            self.confirm_func(name, confirm, callback, name)
        else:
            callback(name)


class ActionMixin:
    signals = ['action']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._actions_enabled = True
        self._actions = []
        self._action_map = {}
        self._internal_action_map = {}
        self.filter_edit_key = None

    def add_actions(self, actions):
        """Adds the actions from an iterable. No check for duplicate entries
        is done. Each action is a dictionary with these keys:
        name - name of the action; must be always present.
        prio - help priority, greater means more important; optional.
        help - help text; if not present, this action will not show in the
        bottom bar.
        long_help - help text for the help popup; if not present, the 'help'
        key will be used.
        internal - if present and True, the action is supposed to be
        consumed internally by calling the translate_internal_action method.
        All extra keys will be sent as the second argument to the signal."""
        for data in actions:
            keys = settings.key_map[data['name']]
            if self.filter_edit_key:
                keys = [k for k in keys if self.filter_edit_key(k)]
            if not keys:
                continue
            help_keys = [display_keys.get(key, key) for key in keys]
            help_text = data.get('help')
            long_text = data.get('long_help') or help_text
            extra = { key: data[key] for key in data
                      if key not in ('key', 'name', 'prio', 'help', 'long_help', 'internal') }
            action = { 'name': data['name'],
                       'help_keys': help_keys,
                       'help': help_text,
                       'long_help': long_text,
                       'priority': data.get('prio', -1),
                       'extra': extra,
                       'enabled': True }
            self._actions.append(action)
            action_map = (self._internal_action_map if data.get('internal', False)
                          else self._action_map)
            for key in keys:
                action_map[key] = action

    def set_action_enabled(self, action_name, enabled):
        for action in self._actions:
            if action['name'] == action_name:
                action['enabled'] = enabled
                return
        raise KeyError('{} not found in actions'.format(action_name))

    def set_all_actions_enabled(self, enabled):
        self._actions_enabled = enabled

    def collect_help(self, data):
        if not self._actions_enabled:
            return
        for action in self._actions:
            if not action['help'] and not action['long_help']:
                continue
            priority = action['priority']
            if priority not in data:
                data[priority] = []
            data[priority].append((action['enabled'], action['help_keys'],
                                   action['help'], action['long_help']))

    def translate_internal_action(self, key):
        if key in self._internal_action_map:
            if not self._actions_enabled:
                return key
            action = self._internal_action_map[key]
            if action['enabled']:
                return action['name']
        return key

    def keypress_action(self, key):
        if key in self._action_map:
            if not self._actions_enabled:
                return key
            action = self._action_map[key]
            if action['enabled']:
                args = [action['name']]
                if action['extra']:
                    args.append(action['extra'])
                urwid.emit_signal(self, 'action', *args)
                return None
        return key

    def keypress(self, size, key):
        key = self.keypress_action(key)
        if key is None:
            return None
        return super().keypress(size, key)


class TraversableListBox(ActionMixin, urwid.ListBox):
    focus_traverse = False


class MaxMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.max_size = None

    def set_max_size(self, index, max_size):
        """Set the max width/height of the widget at index. Only a single
        widget can have max size. Note that when redistributing the extra
        space, the weights are not taken into account. If we ever start
        using the weights, this would have to be fixed."""
        self.max_size = (index, max_size)
        self._invalidate()

    def adjust_size(self, sizes):
        if not self.max_size:
            return sizes

        index, max_size = self.max_size
        if sizes[index] <= max_size:
            return sizes

        to_enlarge = []
        for i, (_, s) in enumerate(self.contents):
            if i != index and s[0] not in (urwid.widget.GIVEN, urwid.widget.PACK):
                to_enlarge.append(i)

        add, rest = divmod(sizes[index] - max_size, len(to_enlarge))
        for i in to_enlarge:
            sizes[i] += add
            if rest:
                sizes[i] += 1
                rest -= 1
        sizes[index] = max_size
        return sizes


class ColumnsMax(MaxMixin, urwid.Columns):
    def column_widths(self, size, focus=False):
        widths = self.adjust_size(super().column_widths(size, focus))
        self._cache_column_widths = widths
        return widths


class PileMax(MaxMixin, urwid.Pile):
    def get_item_rows(self, size, focus):
        return self.adjust_size(super().get_item_rows(size, focus))


class Workplace(ActionMixin, urwid.WidgetPlaceholder):
    signals = ['focus', 'help']

    def refocus(self):
        try:
            self.original_widget.base_widget.focus_position = 0
        except IndexError:
            return
        urwid.emit_signal(self, 'focus')

    def get_focus_path_w(self):
        """A version of get_focus_path that respects the focus_traverse
        attribute and returns also the widgets."""
        out = []
        w = self.original_widget.base_widget
        while True:
            if hasattr(w, 'focus_traverse') and not w.focus_traverse:
                return out
            try:
                p = w.focus_position
            except IndexError:
                return out
            out.append((w, p))
            w = w.focus.base_widget

    def set_focus_path_w(self, path, to_last=False):
        """A version of set_focus_path that also focuses child widgets after
        the given path to their first selectable child (or last if to_last
        is True), stopping at focus_traverse=False."""
        w = self.original_widget.base_widget
        for _, p in path:
            if p != w.focus_position:
                w.focus_position = p
            w = w.focus.base_widget
        while True:
            if hasattr(w, 'focus_traverse') and not w.focus_traverse:
                return
            try:
                i = len(w.contents) - 1 if to_last else 0
                while True:
                    w.focus_position = i
                    if w.focus.selectable():
                        break
                    i = i - 1 if to_last else i + 1
            except (IndexError, AttributeError):
                return
            w = w.focus.base_widget

    def get_full_focus_path_w(self):
        """Returns the path of widgets in focus, including decorations."""
        out = []
        w = self.original_widget
        while True:
            out.append(w)
            if hasattr(w, 'focus_traverse') and not w.focus_traverse:
                return out
            if hasattr(w, 'original_widget'):
                w = w.original_widget
                continue
            w = w.focus
            if not w:
                return out

    def find_path_w(self, widget):
        """Search for the given widget in the whole tree, respecting
        focus_traverse. Return the path to the widget (may be []) or
        None if the widget was not found."""
        path = []
        w, pos = self.original_widget.base_widget, 0
        while True:
            try:
                child = w.contents[pos][0]
            except (IndexError, TypeError, AttributeError):
                if len(path) == 0:
                    return None
                w, pos = path.pop()
                pos += 1
                continue
            while True:
                if child == widget:
                    path.append((w, pos))
                    return path
                if not hasattr(child, 'original_widget'):
                    break
                child = child.original_widget
            if not child.selectable():
                pos += 1
                continue
            if hasattr(child, 'focus_traverse') and not child.focus_traverse:
                pos += 1
                continue
            path.append((w, pos))
            w, pos = child, 0

    def focus_next(self, prev=False):
        path = self.get_focus_path_w()
        done = False
        while len(path):
            w, p = path[-1]
            try:
                while True:
                    p = p - 1 if prev else p + 1
                    path[-1] = (w, p)
                    self.set_focus_path_w(path, prev)
                    if w.focus.selectable():
                        urwid.emit_signal(self, 'focus')
                        return
            except IndexError:
                path = path[:-1]
        self.set_focus_path_w(path, prev)
        urwid.emit_signal(self, 'focus')

    def focus_to(self, widget):
        path = self.find_path_w(widget)
        if path is not None:
            self.set_focus_path_w(path)
            urwid.emit_signal(self, 'focus')
            return True
        return False

    def get_focus(self):
        try:
            w, _ = self.get_focus_path_w()[-1]
        except IndexError:
            return None
        return w.focus.base_widget

    def get_help(self, long=False):
        data = {}
        for w in reversed(self.get_full_focus_path_w()):
            if hasattr(w, 'collect_help'):
                w.collect_help(data)

        if not data:
            return ''

        result = []
        if not long:
            result.append(('help-key', ' {} '.format(settings.key_map['help'][0])))
            result.append(('help-desc', ' help  '))
        for prio in sorted(data.keys(), reverse=True):
            for enabled, keys, desc, long_desc in data[prio]:
                if not long and not desc:
                    continue
                if not long and not enabled:
                    continue
                if long and not long_desc:
                    continue
                if long:
                    for i, key in enumerate(keys):
                        if i > 0:
                            result.append(('help-desc', ', '))
                        result.append(('help-key', ' {} '.format(key)))
                    state = '' if enabled else ' [disabled]'
                    result.append(('help-desc', ' {}{}\n'.format(long_desc, state)))
                else:
                    result.append(('help-key', ' {} '.format(keys[0])))
                    result.append(('help-desc', ' {}  '.format(desc)))
        return result

    def keypress(self, size, key):
        if key in settings.key_map['focus_next'] and self._actions_enabled:
            self.focus_next()
            return None
        if key in settings.key_map['focus_prev'] and self._actions_enabled:
            self.focus_next(prev=True)
            return None
        if key in settings.key_map['help'] and self._actions_enabled:
            urwid.emit_signal(self, 'help', self.get_help(long=True) or [])
        return super().keypress(size, key)


class PassiveAttrMap(urwid.AttrMap):
    def __init__(self, w, attr_map, focus_map, passive_map):
        self._passive = False
        super().__init__(w, attr_map, focus_map)
        self.set_passive_map(passive_map)

    def set_attr_map(self, attr_map):
        super().set_attr_map(attr_map)
        self._orig_attr_map = self._attr_map
        if self._passive:
            self._attr_map = self._passive_map

    def set_passive_map(self, passive_map):
        orig = self._focus_map
        super().set_focus_map(passive_map)
        self._passive_map = self._focus_map
        self._focus_map = orig

    def set_passive(self, enabled):
        if self._passive == enabled:
            return
        if enabled:
            self._attr_map = self._passive_map
        else:
            self._attr_map = self._orig_attr_map
        self._passive = enabled
        self._invalidate()


class PrettyListWalker(urwid.SimpleFocusListWalker):
    def __init__(self, content, focus_map, passive_map):
        items = []
        for c in content:
            w = urwid.SelectableIcon(c)
            w.set_wrap_mode('ellipsis')
            items.append(PassiveAttrMap(w, 'list', focus_map, passive_map))
        self._ext_callback = None
        super().__init__(items)
        if self.focus is not None:
            self._set_passive(self.focus, True)

    def _set_passive(self, index, enabled):
        try:
            widget = self[index]
        except IndexError:
            assert not enabled
            # this may happen when we're refocusing a changed list
            return
        if hasattr(widget, 'set_passive'):
            widget.set_passive(enabled)

    def _focus_changed(self, new_focus):
        self._set_passive(self.focus, False)
        self._set_passive(new_focus, True)
        if self._ext_callback:
            self._ext_callback(new_focus)

    def set_focus_changed_callback(self, callback):
        self._ext_callback = callback


class SepTextLayout(urwid.StandardTextLayout):
    def layout(self, text, width, align, wrap):
        segs = super().layout(text, width, align, wrap)
        for i, seg in enumerate(segs):
            if len(seg[0]) != 3 or type(seg[0][2]) != int:
                continue
            if text[seg[0][1]] != '\0':
                continue
            fill = text[seg[0][1] + 1]
            segs[i] = [(width, seg[0][1], fill * width)]
        return segs


class BoxedEdit(ActionMixin, urwid.Edit):
    signals = ['moved']

    def __init__(self, readonly=False, **kwargs):
        self._readonly = readonly
        self._viewport_x = 0
        self._viewport_y = None
        self._undo_buffer = []
        self._undo_pos = -1
        self._undo_collect = False
        if 'caption' in kwargs:
            raise AttributeError('BoxedEdit does not support caption')
        self._keep_cache = False
        self._needs_moved_signal = False
        if not 'multiline' in kwargs:
            kwargs['multiline'] = True
        super().__init__(**kwargs)
        actions = [{ 'name': 'select', 'internal': True, 'help': 'select text',
                     'long_help': 'text selection; Enter to copy, Esc to unselect' }]
        if not readonly:
            actions.append({ 'name': 'paste', 'internal': True, 'help': 'paste',
                             'long_help': 'paste previously copied text' })
            actions.append({ 'name': 'undo', 'internal': True, 'help': 'undo' })
            actions.append({ 'name': 'redo', 'internal': True, 'help': 'redo' })
            actions.append({ 'name': 'delete-line', 'internal': True })
            self.filter_edit_key = lambda key: not self.valid_edit_key(key)
        self.add_actions(actions)

    readonly = property(lambda self: self._readonly)

    def set_layout(self, align, wrap, layout=None):
        if layout is None and self._readonly:
            layout = SepTextLayout()
        super().set_layout(align, wrap, layout)

    def emit_moved(self, x, y):
        if not self._needs_moved_signal:
            return
        self._needs_moved_signal = False
        urwid.emit_signal(self, 'moved', self.edit_pos, x, y)

    def set_edit_text(self, text):
        self.add_undo(edit=True)
        super().set_edit_text(text)

    edit_text = property(lambda self: self.get_edit_text(), set_edit_text)

    def set_initial_edit_text(self, text):
        # Can't reset the viewport in set_edit_text, that function is called
        # for text input keypresses. We also don't want to put the initial
        # text in the undo buffer.
        super().set_edit_text(text)
        self._viewport_x = 0
        self._viewport_y = None
        self._undo_buffer = []
        self._undo_pos = -1
        self._undo_collect = False

    def set_markup_text(self, markup):
        text, self._attrib = urwid.util.decompose_tagmarkup(markup)
        self._viewport_y = None
        self.set_initial_edit_text(text)
        try:
            self.links = markup.links
        except AttributeError:
            self.links = None

    def insert_text(self, text):
        super().insert_text(text)
        # Need to reset the horizontal position of the viewport in case the
        # line wraps. We want the screen to jump to the left on line wrap.
        # If we don't wrap, the viewport will be adjusted; however, if we
        # ever start using a multiline non-wrapping edit widget, this
        # behavior will not be the most pleasant. We'll have to figure out
        # a different workaround in such case.
        self._viewport_x = 0

    def render(self, size, focus=False):
        if len(size) == 1:
            cols = size[0]
            rows = None
        else:
            cols, rows = size
        text, attrs = self.get_text()

        trans = urwid.Text.get_line_translation(self, cols, (text, attrs))
        text_rows = len(trans)
        # calculate raw cursor coordinates
        cx, cy = urwid.text_layout.calc_coords(text, trans, self.edit_pos)
        # adjust the viewport vertically
        if rows is None or text_rows <= rows:
            self._viewport_y = 0
        else:
            if self._viewport_y is None:
                # start with the cursor in the middle of the screen
                self._viewport_y = cy - rows // 2

            # if cursor is below the viewport, move the viewport down
            self._viewport_y = max(self._viewport_y, cy - rows + 1)
            # if cursor is above the viewport, move the viewport up
            self._viewport_y = min(self._viewport_y, cy)
            # don't move the viewport above or below the text
            self._viewport_y = max(self._viewport_y, 0)
            self._viewport_y = min(self._viewport_y, text_rows - rows)

        # adjust the viewport horizontally
        self._viewport_x = max(self._viewport_x, cx - cols + 1)
        self._viewport_x = min(self._viewport_x, cx)
        self._viewport_x = max(self._viewport_x, 0)

        visible_trans = []
        y = self._viewport_y
        if rows is None:
            y_stop = text_rows
        else:
            y_stop = min(y + rows, text_rows)
        while y < y_stop:
            t = trans[y]
            if self._viewport_x > 0:
                t = urwid.text_layout.shift_line(t, -self._viewport_x)
            visible_trans.append(t)
            y += 1

        if self.highlight:
            attrs = self.apply_highlight(attrs)
        canv = urwid.canvas.apply_text_layout(text, attrs, visible_trans, cols)
        canv = urwid.CompositeCanvas(canv)
        if focus:
            canv.cursor = (cx - self._viewport_x, cy - self._viewport_y)

        if rows is not None and text_rows <= rows:
            # pad to the required height
            canv.pad_trim_top_bottom(0, rows - text_rows)

        self.emit_moved(cx, cy)
        return canv

    def apply_highlight(self, attrs):
        start, end = self.highlight
        # do not highlight the character at the cursor, it looks weird
        if start < self.highlight_origin:
            start += 1
        else:
            end -= 1
        if start >= end:
            return attrs

        result = []
        pos = 0
        replace = 0
        for a, run in attrs:
            if pos < start:
                if pos + run <= start:
                    result.append((a, run))
                    pos += run
                    continue
                eat = start - pos
                if eat:
                    result.append((a, eat))
                    pos += eat
                    run -= eat
            if pos < end:
                if pos + run < end:
                    pos += run
                    replace += run
                    continue
                eat = end - pos
                replace += eat
                pos += eat
                run -= eat
            if replace:
                result.append(('select', replace))
                replace = 0
            if run:
                result.append((a, run))
                pos += run
        if pos < start:
            result.append((None, start - pos))
            pos = start
        if pos < end:
            result.append(('select', end - pos))
        return result

    def delete_line(self):
        if self._delete_highlighted():
            return
        start = self.edit_text.rfind('\n', 0, self.edit_pos) + 1
        end = self.edit_text.find('\n', self.edit_pos) + 1 or len(self.edit_text)
        self.set_edit_text(self.edit_text[:start] + self.edit_text[end:])
        self.set_edit_pos(self.edit_pos)

    def _invalidate(self, keep_cache=False):
        prev = self._cache_maxcol
        super()._invalidate()
        if self._keep_cache or keep_cache:
            self._cache_maxcol = prev

    def position_coords(self, maxcol, pos):
        if pos == 0:
            # Optimize the case of move_cursor_to_coords adjusting for the
            # caption. We don't support caption and we want move_cursor_to_coords
            # to be fast.
            return 0, 0
        return super().position_coords(maxcol, pos)

    def get_pref_col(self, size):
        maxcol = size[0]
        if self.pref_col_maxcol[1] != maxcol:
            x, y = self.position_coords(maxcol, self.edit_pos)
            # Cache the result for better performance.
            if type(self.pref_col_maxcol[0]) == str:
                # preserve 'left' and 'right'
                self.pref_col_maxcol = (self.pref_col_maxcol[0], maxcol)
            else:
                # store the calculated value for int and None
                self.pref_col_maxcol = (x, maxcol)
            self.cached_coords = (x, y)
            return x
        return self.pref_col_maxcol[0]

    def get_cursor_coords(self, size):
        # cached_coords is valid only when pref_col_maxcol is set.
        # get_pref_col recalculates the cache for us if it's not set.
        self.get_pref_col(size)
        return self.cached_coords

    def move_cursor_to_coords(self, size, x, y):
        # movement of the cursor does not invalidate the cached line translation
        prev_cache = self._keep_cache
        self._keep_cache = True
        result = super().move_cursor_to_coords(size, x, y)
        self._keep_cache = prev_cache
        if result:
            real_x = x
            if real_x == 'left':
                real_x = 0
            elif real_x == 'right':
                real_x, _ = self.position_coords(size[0], self.edit_pos)
            self.cached_coords = (real_x, y)
            self.emit_moved(x, y)
        return result

    def set_edit_pos(self, pos):
        self.add_undo(move=True)
        # movement of the cursor does not invalidate the cached line translation
        prev_cache = self._keep_cache
        self._keep_cache = True
        self._needs_moved_signal = True
        highlight = self.highlight
        super().set_edit_pos(pos)
        self._keep_cache = prev_cache
        self.update_highlight(highlight)

    edit_pos = property(lambda self: self._edit_pos, set_edit_pos)

    def start_highlight(self):
        self.highlight = (self.edit_pos, self.edit_pos)
        self.highlight_origin = self.edit_pos

    def update_highlight(self, highlight):
        self.highlight = highlight
        if not self.highlight:
            return
        if self.edit_pos < self.highlight_origin:
            self.highlight = (self.edit_pos, self.highlight_origin + 1)
        else:
            self.highlight = (self.highlight_origin, self.edit_pos + 1)

    def close_undo_slot(self):
        # Undo slot is open if _undo_pos points behind the last slot.
        if self._undo_pos == len(self._undo_buffer):
            self._undo_pos -= 1

    def set_undo_slot(self):
        if self._undo_pos == len(self._undo_buffer):
            # opened slot, do nothing
            return
        if self._undo_pos < len(self._undo_buffer) - 1:
            # ditch redo
            self._undo_buffer = self._undo_buffer[:self._undo_pos + 1]
        self._undo_buffer.append((self.edit_text, self.edit_pos))
        # keep the slot open
        self._undo_pos = len(self._undo_buffer)

    def add_undo(self, edit=False, move=False, collected=False):
        if self._undo_collect == 'ignore':
            return
        if collected:
            assert not edit and not move
            collect = self._undo_collect
            self._undo_collect = False
            if not collect or 'move' not in collect:
                # Not moved during keypress handling. Nothing to do. (If
                # there was edit, it was already handled.)
                return
            if 'edit' in collect:
                # Both moved and edited during keypress handling. The edit
                # was handled, the move we ignore.
                return
            # Only moved without an edit. We need to close the current undo
            # buffer.
            move = True
        if self._undo_collect is False:
            if move:
                self.close_undo_slot()
        else:
            if edit:
                self._undo_collect.add('edit')
            if move:
                self._undo_collect.add('move')
        if edit:
            self.set_undo_slot()

    def undo(self):
        if self._undo_pos < 0:
            # nothing to undo
            return
        self.close_undo_slot()
        if self._undo_pos == len(self._undo_buffer) - 1:
            # we're starting undo, store the current state for redo
            self._undo_buffer.append((self.edit_text, self.edit_pos))
        self._undo_collect = 'ignore'
        self.edit_text, self.edit_pos = self._undo_buffer[self._undo_pos]
        self._undo_pos -= 1
        self._undo_collect = False

    def redo(self):
        if self._undo_pos >= len(self._undo_buffer) - 2:
            # nothing to redo; we never increment to point to the last slot
            return
        self._undo_pos += 1
        self._undo_collect = 'ignore'
        self.edit_text, self.edit_pos = self._undo_buffer[self._undo_pos + 1]
        self._undo_collect = False

    def valid_edit_key(self, key):
        return (self.valid_char(key) or
                key in ('enter', 'backspace', 'delete'))

    def keypress(self, size, key):
        flow_size = (size[0],)
        self._undo_collect = set()

        if not self._readonly and self.valid_edit_key(key):
            urwid.Edit.keypress(self, flow_size, key)
            key = None
        else:
            key = self.translate_internal_action(key)
            key = self._command_map[key]
        if key in (CURSOR_LEFT,
                   CURSOR_RIGHT,
                   CURSOR_UP,
                   CURSOR_DOWN,
                   CURSOR_MAX_LEFT,
                   CURSOR_MAX_RIGHT):
            highlight = self.highlight
            key = super().keypress(flow_size, key)
            if key is None:
                self.update_highlight(highlight)
        elif (key in (CURSOR_PAGE_UP, CURSOR_PAGE_DOWN) or
              (self._readonly and key == ' ')):
            x, y = self.get_cursor_coords(flow_size)
            pref_col = self.get_pref_col(flow_size)
            if key == CURSOR_PAGE_UP:
                y -= size[1]
            else:
                y += size[1]

            trans = self.get_line_translation(flow_size[0])
            y = min(max(y, 0), len(trans) - 1)

            self.move_cursor_to_coords(flow_size, pref_col, y)
            key = None
        elif key in ('cursor top', 'cursor bottom'):
            pref_col = self.get_pref_col(flow_size)
            if key == 'cursor top':
                y = 0
            else:
                y = len(self.get_line_translation(flow_size[0])) - 1

            self.move_cursor_to_coords(flow_size, pref_col, y)
            key = None
        elif self.highlight and key in ('esc', 'activate'):
            if key == 'activate':
                clipboard.text = self.edit_text[self.highlight[0]:self.highlight[1]]
            self.highlight = None
            self._invalidate(keep_cache=True)
            key = None
        elif key == 'select':
            self.start_highlight()
            self._invalidate(keep_cache=True)
            key = None
        elif key == 'delete-line':
            self.delete_line()
            key = None
        elif key == 'paste':
            if clipboard.text:
                self.insert_text(clipboard.text)
            key = None
        elif key == 'undo':
            self.undo()
            key = None
        elif key == 'redo':
            self.redo()
            key = None

        self.emit_moved(*self.get_cursor_coords(flow_size))
        self.add_undo(collected=True)

        if key:
            key = self.keypress_action(key)
        return key

    def mouse_event(self, size, event, button, x, y, focus):
        flow_size = (size[0],)
        if button == 1:
            return self.move_cursor_to_coords(flow_size, x + self._viewport_x,
                                              y + self._viewport_y)


class CoupledViewer(BoxedEdit):
    def __init__(self, coupler, **kwargs):
        self._coupler = coupler
        self._coupler.register(self)
        self._dont_emit = False
        kwargs['wrap'] = 'clip'
        kwargs['readonly'] = True
        super().__init__(**kwargs)

    def emit_moved(self, x, y):
        if self._dont_emit:
            return
        super().emit_moved(x, y)

    def coupled_move(self, x, y):
        self._dont_emit = True
        # In the 'clip' wrap mode, the widget width is not important. Try to
        # reuse the cached width; if not cached, use 1.
        maxcol = self.pref_col_maxcol[1] or 1
        self.move_cursor_to_coords((maxcol,), x, y)
        self._dont_emit = False
        self._needs_moved_signal = False


class Coupler:
    def __init__(self):
        self._viewers = weakref.WeakSet()

    def register(self, viewer):
        self._viewers.add(viewer)
        urwid.connect_signal(viewer, 'moved', self._moved, weak_args=(viewer,))

    def _moved(self, source, position, x, y):
        for viewer in self._viewers:
            if viewer == source:
                continue
            viewer.coupled_move(x, y)


class Placeholder(ActionMixin, urwid.WidgetPlaceholder):
    pass


class JailedPlaceholder(Placeholder):
    """A placeholder widget that does not allow arrow keys to escape. Used
    as a container for widgets that should be focused using the Tab key
    only."""
    def keypress(self, size, key):
        key = super().keypress(size, key)
        if self._command_map[key] in (CURSOR_LEFT,
                                      CURSOR_RIGHT,
                                      CURSOR_UP,
                                      CURSOR_DOWN,
                                      CURSOR_MAX_LEFT,
                                      CURSOR_MAX_RIGHT,
                                      CURSOR_PAGE_UP,
                                      CURSOR_PAGE_DOWN):
            key = None
        return key


class FixedButton(urwid.Button):
    def pack(self, size, focus=False):
        return (len(self._label.text) + 4, 1)


class Popup(ActionMixin, urwid.Overlay):
    def __init__(self, callback, workplace, attr_map=None, focus_map=None, height=None,
                 callback_args=None, popup_widget=None, overlay_attr_map=None):
        self._workplace = workplace
        self._attr_map = attr_map
        self._focus_map = focus_map
        self._callback = callback
        self._callback_args = callback_args or ()
        self._default_button = None
        self._default_esc_button = None
        self._shortcuts = {}
        self._data_widgets = {}
        if height == 'max':
            parms = { 'align': 'center',
                      'width': ('relative', 50),
                      'valign': 'middle',
                      'height': ('relative', 100),
                    }
        elif height == 'full':
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': ('relative', 100),
                    }
        elif height is None:
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': 'pack',
                    }
        else:
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': ('relative', 0),
                      'min_height': height + 2,
                    }
        super().__init__(None, self._workplace.original_widget,
                         attr_map=overlay_attr_map, **parms)
        if popup_widget:
            self._setup(popup_widget)

    def new_button(self, label):
        if type(label) == str:
            index = label.find('~')
            if index >= 0:
                shortcut = label[index + 1]
                label = [label[:index], ('button-shortcut', shortcut), label[index+2:]]
                self._shortcuts[shortcut] = urwid.util.decompose_tagmarkup(label)[0]
        return urwid.AttrMap(FixedButton(label, on_press=self._handler),
                             self._attr_map, self._focus_map)

    def new_edit(self, name, edit_text):
        widget = urwid.Edit(edit_text=edit_text)
        self._data_widgets[name] = widget
        return widget

    def register_widget(self, name, widget):
        self._data_widgets[name] = widget
        return widget

    def set_default_button(self, label, label_esc=None):
        """Sets the button that is reported when Enter is pressed in one of
        the edit boxes. Similarly for Esc. Note that the label does not need
        to correspond to a real button. As a special case, if label or
        label_esc is True, the Enter/Esc is functional but the button label
        is not reported to the callback. Edit data is reported to the
        callback only if a non-Esc button was pressed."""
        self._default_button = label
        self._default_esc_button = label_esc

    def _setup(self, widget):
        self.popup_widget = widget
        self.top_w = urwid.LineBox(widget)

    def show(self):
        self._workplace.original_widget = self
        self._workplace.set_all_actions_enabled(False)

    def start(self, widget):
        self._setup(widget)
        self.show()

    def _stop(self):
        self._workplace.original_widget = self.contents[0][0]
        self._workplace.set_all_actions_enabled(True)

    def stop(self):
        self._stop()
        self._callback(*self._callback_args)

    def keypress(self, size, key):
        if key == 'enter':
            focus = self.get_focus_widgets()
            handle = not focus
            for f in focus:
                f = f.base_widget
                if f in self._data_widgets.values():
                    handle = f
                    break
            if handle and self._handler(key=key, focus=f if handle is not True else None):
                return None
        elif key == 'esc':
            if self._handler(key=key):
                return None
        elif key in self._shortcuts:
            if self._handler(key=key):
                return None
        return super().keypress(size, key)

    def _handler(self, button=None, key=None, focus=None):
        collect_data = True
        if button is not None:
            button = urwid.util.decompose_tagmarkup(button.label)[0]
            if button == self._default_esc_button:
                collect_data = False
        if key is not None:
            if key == 'enter' and self._default_button:
                button = self._default_button
            elif key == 'esc' and self._default_esc_button:
                button = self._default_esc_button
                collect_data = False
            elif key in self._shortcuts:
                button = self._shortcuts[key]
                if button == self._default_esc_button:
                    collect_data = False
            else:
                return False

        self._stop()
        data = {}
        if collect_data:
            for k, widget in self._data_widgets.items():
                if isinstance(widget, urwid.Edit):
                    data[k] = widget.get_edit_text()
                elif isinstance(widget, urwid.Pile) and widget == focus:
                    data[k] = widget.focus_position
        if button is not None and button is not True:
            data['button'] = button
        self._callback(*self._callback_args, **data)
        return True
