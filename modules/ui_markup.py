import bisect

class MarkupLinks:
    def __init__(self):
        self.indices = [0]
        self.objects = [None]

    def add(self, index, obj):
        assert self.indices[-1] <= index
        if self.objects[-1] == obj:
            return
        if self.indices[-1] == index:
            self.objects[-1] = obj
            return
        self.indices.append(index)
        self.objects.append(obj)

    def find(self, index):
        pos = bisect.bisect(self.indices, index) - 1
        assert pos >= 0
        return self.objects[pos]


class Markup(list):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.text_len = 0
        self.links = MarkupLinks()

    def link_start(self, obj):
        self.links.add(self.text_len, obj)

    def link_stop(self):
        self.links.add(self.text_len, None)

    def _update_text_len(self, item):
        if isinstance(item, tuple):
            s = item[1]
        elif isinstance(item, str):
            s = item
        else:
            raise ValueError('Unexpected markup type {}'.format(type(item)))
        self.text_len += len(s)

    def append(self, item):
        self._update_text_len(item)
        super().append(item)

    def extend(self, items):
        idx = len(self)
        super().extend(items)
        for i in range(idx, len(self)):
            self._update_text_len(self[i])
