import urllib.request

def escape(num):
    if num < 0x100:
        return '\\x{:02x}'.format(num)
    if num < 0x10000:
        return '\\u{:04x}'.format(num)
    return '\\U{:08x}'.format(num)

def process_east_asian_width(f):
    out = []
    last = None
    for line in f.readlines():
        line = line.decode()
        if line[:1] == "#": continue
        line = line.strip()
        hex,rest = line.split(";",1)
        wid,rest = rest.split(" # ",1)
        word1 = rest.split(" ",1)[0]

        if "." in hex:
            hex = hex.split("..")[1]
        num = int(hex, 16)

        if word1 in ("COMBINING","MODIFIER","<control>"):
            l = 0
        elif wid in ("W", "F"):
            l = 2
        else:
            l = 1

        if last is None:
            out.append((0, l))
            last = l

        if last == l:
            out[-1] = (num, l)
        else:
            out.append( (num, l) )
            last = l

    return ['{}-{}'.format(escape(out[i - 1][0] + 1), escape(o[0]))
            for i, o in enumerate(out)
            if o[1] == 2]

if __name__ == '__main__':
    f = urllib.request.urlopen('http://www.unicode.org/Public/4.0-Update/EastAsianWidth-4.0.0.txt')
    ranges = [r'\x00-\x1f'] + process_east_asian_width(f)
    print(''.join(ranges))
